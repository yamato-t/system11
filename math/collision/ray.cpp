#include "ray.h"

namespace core {

	namespace math {

		namespace collision {

			// -----------------------------------------------------------------------------
			/**
			 * @brief		レイと平面の判定
			 * @param		pPoint		平面上の点
			 * @param		pNormal		正規化済みの面法線
			 * @param		pStart		開始座標
			 * @param		pRay		レイ（無限遠）
			 * @param[out]	pCross		交点
			 * @param[out]	pOffset		交点までのベクトル
			 */
			bool Ray::CrossRayAndPlane(const vec4* pPoint, const vec4* pNormal, const vec4* pStart, const vec4* pRay, vec4* pCross, vec4* pOffset) {
				float fDistance;
				if (!CheckCrossRayAndPlane(pPoint, pNormal, pStart, pRay, &fDistance)) {
					return false;
				}

				if (fDistance < 0.0f) {
					return false;
				}

				Vec4Scale(pOffset, pRay, fDistance);
				Vec4Add(pCross, pStart, pOffset);

				return true;
			}

			// -----------------------------------------------------------------------------
			/**
			 * @brief		レイとポリゴンの交差判定
			 * @param		pPolygon	ポリゴン
			 * @param		pStart		開始座標
			 * @param		pRay		レイ（無限遠）
			 * @param[out]	pCross		交点
			 * @param[out]	pOffset		交点までのベクトル
			 */
			template <typename _t>
			bool Ray::CrossRayAndPolygon(const _t* pPolygon, const vec4* pStart, const vec4* pRay, vec4* pCross, vec4* pOffset) {
				vec4 vNormal;
				GetNormal(&vNormal, pPolygon);

				vec4 vCross, vOffset;
				if (!CrossRayAndPlane(pPolygon->m_vVertex[0], &vNormal, pStart, pRay, &vCross, &vOffset)) {
					// 平面を貫いてすらいない
					return false;
				}

				if (!CheckPointInPolygon(pPolygon, &vCross)) {
					// 平面上で貫いている点が内包されていない
					return false;
				}

				*pCross = vCross;
				*pOffset = vOffset;

				return false;
			}

			// -----------------------------------------------------------------------------
			/**
			 * @brief		レイと平面の判定(基本部分)
			 * @param		pPoint		平面上の点
			 * @param		pNormal		正規化済みの面法線
			 * @param		pStart		開始座標
			 * @param		pRay		レイ（無限遠）
			 * @param[out]	pLength		交点までの長さ
			 * @return		交差していればtrue
			 */
			bool Ray::CheckCrossRayAndPlane(const vec4* pPoint, const vec4* pNormal, const vec4* pStart, const vec4* pRay, float* pLength) {

				float fDot = Vec4Dot(pNormal, pRay);

				if (fDot >= 0.0f) {
					// ぶつかる事がない
					return false;
				}

				vec4 vDistance;
				Vec4Subtract(&vDistance, pPoint, pStart);
				float fP = Vec4Dot(pNormal, &vDistance);
				fP *= (1.0f / fDot);
				*pLength = fP;

				return true;
			}
		}
	}
}