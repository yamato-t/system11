
#include "sphere.h"


namespace core {

	namespace math {
	
		namespace collision {

			// -----------------------------------------------------------------------------
			/**
			 * @brief		球とレイの交差
			 * @param		pSphere		球
			 * @param		pStart		開始座標
			 * @param		pRay		レイ（無限遠）
			 * @param[out]	pCross		交点
			 * @param[out]	pOffset		交点までのオフセット
			 * @return		交差していればtrue
			 */
			bool Sphere::CrossSphereAndRay(const SPHERE* pSphere, const vec4* pStart, const vec4* pRay, vec4* pCross, vec4* pOffset) {
				float fP;
				if (!CheckCrossSphereAndRay(pSphere, pStart, pRay, &fP)) {
					return false;
				}

				Vec4Scale(pOffset, pRay, fP);
				Vec4Add(pCross, pStart, pOffset);

				return true;
			}

			// -----------------------------------------------------------------------------
			/**
			 * @brief			球とベクトルの交差
			 * @param			pSphere		球
			 * @param			pStart		開始座標
			 * @param			pDir		ベクトル
			 * @param[in,out]	pLength		ベクトルの長さ、ヒット時は交点までの長さに更新
			 * @param[out]		pCross		交点
			 * @param[out]		pOffset		交点までのオフセット
			 * @return			交差していればtrue
			 */
			bool Sphere::CrossSphereAndDir(const SPHERE* pSphere, const vec4* pStart, const vec4* pDir, float* pLength, vec4* pCross, vec4* pOffset) {

				float fLength = *pLength;

				float fP = 0;
				if (!CheckCrossSphereAndDir(pSphere, pStart, pDir, fLength, &fP)) {
					return false;
				}

				Vec4Scale(pOffset, pDir, fP);
				Vec4Add(pCross, pStart, pOffset);
				*pLength = fP;

				return true;
			}

			// -----------------------------------------------------------------------------
			/**
			 * @brief		単純な球と球の衝突判定
			 * @param		pSphere1	球
			 * @param		pSphere2	球
			 * @param[out]	pCross		球2の座標
			 * @param[out]	pOffset		球1と球2の距離
			 * @return		交差していればtrue
			 */
			bool Sphere::HitSphereAndSphere(const SPHERE* pSphere1, const SPHERE* pSphere2, vec4* pCross, vec4* pOffset) {
				float fR = pSphere1->m_fRadius + pSphere2->m_fRadius;

				*pCross = pSphere2->m_vCenter;
				Vec4Subtract(pOffset, &pSphere2->m_vCenter, &pSphere1->m_vCenter);

				return (Vec4Distance(&pSphere1->m_vCenter, &pSphere2->m_vCenter) < fR);
			}


			// -----------------------------------------------------------------------------
			/**
			 * @brief		移動球と球の衝突判定
			 * @param		pMSphere	移動球
			 * @param		pMove		移動ベクトル
			 * @param		pSphere		球
			 * @param[out]	pCross		ぶつかった座標
			 * @param[out]	pOffset		座標までのベクトル
			 * @return		交差していればtrue
			 */
			bool Sphere::HitMSphereAndSphere(const SPHERE* pMSphere, const vec4* pMove, const SPHERE* pSphere, vec4* pCross, vec4* pOffset) {
				SPHERE xChecked = *pSphere;
				xChecked.m_fRadius += pMSphere->m_fRadius;
				if (CrossSphereAndRay(&xChecked, &pMSphere->m_vCenter, pMove, pCross, pOffset)) {
					Vec4Subtract(pCross, &pMSphere->m_vCenter, pOffset);
					return true;
				};

				return false;
			}

			// -----------------------------------------------------------------------------
			/**
			 * @brief			移動球同士の衝突判定
			 * @param			pMSphere1	移動球1
			 * @param			pMove1		1の移動ベクトル
			 * @param			pMSphere2	移動球2
			 * @param			pMove2		2の移動ベクトル
			 * @param[out]		pCross		ぶつかった座標
			 * @param[out]		pOffset		座標までのベクトル
			 * @return			交差していればtrue
			 */
			bool Sphere::HitMSphereAndMSphere(const SPHERE* pMSphere1, const vec4* pMove1, const SPHERE* pMSphere2, const vec4* pMove2, vec4* pCross, vec4* pOffset) {
				vec4 vMove;
				Vec4Subtract(&vMove, pMove1, pMove2);

				return HitMSphereAndSphere(pMSphere1, &vMove, pMSphere2, pCross, pOffset);
			}

			// -----------------------------------------------------------------------------
			/**
			 * @brief			移動球とポリゴンの衝突判定
			 * @param			pMSphere	移動球
			 * @param			pMove		移動ベクトル
			 * @param			pPolygon	ポリゴン
			 * @param[out]		pCross		接触時の球の座標
			 * @param[out]		pOffset		接触時の球の座標までのオフセット
			 * @return			交差していればtrue
			 */
			template <typename _t>
			bool Sphere::HitMSphereAndPolygon(const SPHERE* pMSphere, const vec4* pMove, const _t* pPolygon, vec4* pCross, vec4* pOffset) {

				float fP = 0;

				vec4 vNormal;
				GetNormal(&vNormal, pPolygon);

				if (!CheckHitMSphereAndPlane(pMSphere, pMove, pPolygon->m_vVertex[0], &vNormal, &fP)) {
					return false;
				}

				vec4 vOffset, vCross;
				Vec4Scale(&vOffset, pMove, fP);
				Vec4Add(&vCross, &pMSphere->m_vCenter, &vOffset);

				// 接触点の座標を求める(面と円が接する座標)
				vec4 vContact;
				Vec4Scale(&vContact, &vNormal, -pMSphere->m_fRadius);
				Vec4Add(&vContact, &vCross, &vContact);

				if (!CheckPointInPolygon(pPolygon, vContact)) {
					return false;
				}

				*pCross = vCross;
				*pOffset = vOffset;

				return true;
			}

			// -----------------------------------------------------------------------------
			/**
			 * @brief		球とレイの交差
			 * @param		pShpere		球
			 * @param		pStart		開始座標
			 * @param		pRay		レイ（無限遠）
			 * @param[out]	pLength		交点までの長さ
			 * @return		交差していればtrue
			 */
			bool Sphere::CheckCrossSphereAndRay(const SPHERE* pSphere, const vec4* pStart, const vec4* pRay, float* pLength) {

				vec4 vTemp;
				Vec4Subtract(&vTemp, pStart, &pSphere->m_vCenter);

				float fDot = Vec4Dot(pRay, &vTemp);
				if (0.0f < fDot) {
					//同じ方向向いてる
					return false;
				}

				float fA = Vec4Dot(pRay, pRay);
				if (fA == 0.0f) {
					// Rayがゼロベクトル
					return false;
				}

				float fB = fDot;

				float fC = (Vec4Dot(&vTemp, &vTemp) - (pSphere->m_fRadius * pSphere->m_fRadius));

				// 式に突っ込む
				float fD = (fB * fB) - (fA * fC);

				// fD = 0なら1点で交差
				// 0 < fDなら2点で交差
				if (fD < 0.0f) {
					//　交差していない
					return false;
				}

				// 衝突場所までの距離と、レイの長さの比率を計算
				float fP = (-fB - sqrt(fD)) / fA;

				if (fP < 0.0f || 1.0f < fP) {
					// すでに内側にある、または、ベクトルの長さ以上の場所で交差している
					return false;
				}

				*pLength = fP;
				return true;
			}

			// -----------------------------------------------------------------------------
			/**
			 * @brief		球とベクトルの交差
			 * @param		pShpere		球
			 * @param		pStart		開始座標
             * @param		pDir		単位ベクトル
             * @param		fLengthDir	ベクトル長さ
			 * @param[out]	pLength		交点までの長さ
			 * @return		交差していればtrue
			 */
			bool Sphere::CheckCrossSphereAndDir(const SPHERE* pSphere, const vec4* pStart, const vec4* pDir, float fLengthDir, float* pLength) {

				vec4 vTemp;
				Vec4Subtract(&vTemp, pStart, &pSphere->m_vCenter);

				float fDot = Vec4Dot(pDir, &vTemp);
				if (0.0f < fDot) {
					//同じ方向向いてる
					return false;
				}

				float fB = fDot;

				float fC = (Vec4Dot(&vTemp, &vTemp) - (pSphere->m_fRadius * pSphere->m_fRadius));

				// 式に突っ込む
				float fD = (fB * fB) - fC;

				// fD = 0なら1点で交差
				// 0 < fDなら2点で交差
				if (fD < 0.0f) {
					//　交差していない
					return false;
				}

				// 衝突場所までの距離と、レイの長さの比率を計算
				float fP = (-fB - sqrt(fD));

				if (fP < 0.0f || fLengthDir < fP) {
					// すでに内側にある、または、ベクトルの長さ以上の場所で交差している
					return false;
				}

				*pLength = fP;

				return true;
			}

			// -----------------------------------------------------------------------------
			/**
			 * @brief		球と平面の交差判定
			 * @param		pSphere		球
			 * @param		pPoint		平面上の点
			 * @param		pNormal		平面上の法線
			 * @param[out]	pLength		交点までの長さ
			 * @return		交差していればtrue
			 */
			bool Sphere::CheckCrossSphereAndPlane(const SPHERE* pSphere, const vec4* pPoint, const vec4* pNormal, float* pLength) {
				// 基本の考え方はラインと平面の交差判定と同じ
				vec4 vVec;
				Vec4Subtract(&vVec, &pSphere->m_vCenter, pPoint);

				float fDot = Vec4Dot(&vVec, pNormal);

				if (fDot <= 0.0f) {
					// もうすでに半分以上めり込んでいます
				}

				// 内積で中心点までの距離が出たので半径と比べる
				if (fabs(fDot) <= pSphere->m_fRadius) {
					*pLength = fDot;
					return true;

				}
				return false;
			}

			// -----------------------------------------------------------------------------
			/**
			 * @brief		移動球と平面の交差判定
			 * @param		pMSphere	移動球
			 * @param		pMove		移動ベクトル
			 * @param		pPoint		平面上の点
			 * @param		pNormal		平面上の法線
			 * @param[out]	pLength		交点までの長さ
			 * @return		交差していればtrue
			 */
			bool Sphere::CheckHitMSphereAndPlane(const SPHERE* pMSphere, const vec4* pMove, const vec4* pPoint, const vec4* pNormal, float* pLength) {

				// 移動前は当たっていない状況である事を前提として作成
				float fDot = Vec4Dot(pMove, pNormal);
				if (0.0f <= fDot) {
					return false;
				}

				SPHERE xChecked = *pMSphere;
				Vec4Add(&xChecked.m_vCenter, &xChecked.m_vCenter, pMove);
				if (CheckCrossSphereAndPlane(&xChecked, pPoint, pNormal, pLength)) {
					float fP = (xChecked.m_fRadius - (*pLength)) / fDot;
					*pLength = fP;
					return true;
				}
				return false;
			}


		}
	}
}