#pragma once

#include "../math.h"
#include "sphere.h"
#include "singleton.h"

namespace core {

	namespace math {

		namespace collision {

			//---------------------------------------------------------------------------------
			/**
			 * @brief	
			 * AABB構造体
			 */
			struct AABB {
				vec4 m_vCenter;		///< 中心点
				vec4 m_vDistance;	///< XYZそれぞれの距離
			};

			//---------------------------------------------------------------------------------
			/**
			 * @brief	
			 * OBB構造体
			 */
			struct OBB {
				vec4	m_vCenter;		///< 中心点
				vec4	m_vAxis[3];		///< ローカル軸
				float	m_fRadius[3];	///< 各軸半径
			};

			//---------------------------------------------------------------------------------
			/**
			 * @brief	
			 * BOX形状のコリジョン
			 */
			class Box : public util::Singleton<Box> {

			private:
				friend class util::Singleton<Box>;

			public:
				// ----------------------------------------------------------------------------------------
				/**
				 * @brief			AABBとベクトルの交差
				 * @param			pAabb		AABB
				 * @param			pStart		開始座標
				 * @param			pDir		単位ベクトル
				 * @param[in,out]	pLength		ベクトルの長さ、ヒット時は交点までの長さに更新
				 * @return			交差していればtrue
				 */
				bool CheckCrossAabbAndDir(const AABB* pAabb, const vec4* pStart, const vec4* pDir, float* pLength);

				// ----------------------------------------------------------------------------------------
				/**
				 * @brief		点のAABB内包チェック
				 * @param		pAabb		AABB
				 * @param		pPos		座標
				 * @return		点が内包されていればtrue
				 */
				bool CheckPosInAabb(const AABB* pAabb, const vec4* pPos);


				// ----------------------------------------------------------------------------------------
				/**
				 * @brief			OBBとベクトルの交差
				 * @param			pObb		OBB
				 * @param			pStart		開始座標
				 * @param			pDir		単位ベクトル
				 * @param[in,out]	pLength		ベクトルの長さ、ヒット時は交点までの長さに更新
				 * @param[out]		pNormal		交差する平面の法線
				 * @return			交差していればtrue
				 */
				bool CheckCrossObbAndDir(const OBB* pObb, const vec4* pStart, const vec4* pDir, float* pLength, vec4* pNormal);

				// ----------------------------------------------------------------------------------------
				/**
				 * @brief		OBBとベクトルの交差
				 * @param			pObb		OBB
				 * @param			pStart		開始座標
				 * @param			pDir		単位ベクトル
				 * @param[in,out]	pLength		ベクトルの長さ、ヒット時は交点までの長さに更新
				 * @param[out]		pCross		交点
				 * @param[out]		pOffset		交点までのオフセット
				 * @param[out]		pNormal		当たった面の法線
				 * @return			交差していればtrue
				 */
				bool CrossObbAndDir(const OBB* pObb, const vec4* pStart, const vec4* pDir, float* pLength, vec4* pCross, vec4* pOffset, vec4* pNormal);

				// ----------------------------------------------------------------------------------------
				/**
				 * @brief		OBBと球の当たり
				 * @param		pObb		OBB
				 * @param		pSphere		球
				 * @param[out]	pCross		交点
				 * @param[out]	pOffset		交点までのオフセット
				 * @return	`	当たっていればtrue
				 */
				bool CrossObbAndSphere(const OBB* pObb, const SPHERE* pSphere, vec4* pCross, vec4* pOffset);

			private:
				Box() {}
				virtual ~Box() {}

			};
		}
	}
}