#pragma once

#include "../math.h"
#include "singleton.h"

namespace core {

	namespace math {

		namespace collision {

			//---------------------------------------------------------------------------------
			/**
			 * @brief	
			 * レイのコリジョン
			 */
			class Ray : public util::Singleton<Ray> {
			private:
				friend class util::Singleton<Ray>;

			public:
				// -----------------------------------------------------------------------------
				/**
				 * @brief		レイと平面の判定
				 * @param		pPoint		平面上の点
				 * @param		pNormal		正規化済みの面法線
				 * @param		pStart		開始座標
				 * @param		pRay		レイ（無限遠）
				 * @param[out]	pCross		交点
				 * @param[out]	pOffset		交点までの距離
				 */
				bool CrossRayAndPlane(const vec4* pPoint, const vec4* pNormal, const vec4* pStart, const vec4* pRay, vec4* pCross, vec4* pOffset);

				// -----------------------------------------------------------------------------
				/**
				 * @brief		レイとポリゴンの交差判定
				 * @param		pPolygon	ポリゴン
				 * @param		pStart		開始座標
				 * @param		pRay		レイ（無限遠）
				 * @param[out]	pCross		交点
				 * @param[out]	pOffset		交点までのベクトル
				 */
				template <typename _t>
				bool CrossRayAndPolygon(const _t* pPolygon, const vec4* pStart, const vec4* pRay, vec4* pCross, vec4* pOffset);

			private:
				// -----------------------------------------------------------------------------
				/**
				 * @brief		レイと平面の判定(基本部分)
				 * @param		pPoint		平面上の点
				 * @param		pNormal		正規化済みの面法線
				 * @param		pStart		開始座標
				 * @param		pRay		レイ（無限遠）
				 * @param[out]	pP			交点までの長さ
				 * @return		交差していればtrue
				 */
				bool CheckCrossRayAndPlane(const vec4* pPoint, const vec4* pNormal, const vec4* pStart, const vec4* pRay, float* pP);

			private:
				Ray() {}
				virtual ~Ray() {}
			};
		}
	}
}