﻿#pragma once

#include "singleton.h"
#include "renderManager.h"
#include "../obj/drawObj.h"

#include "../camera/cameraOrtho.h"
#include "../post/proc.h"

namespace core {

	namespace render {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * ポスト処理クラス
		 */
		class PostProcRender	: public Render
								, public util::Singleton<PostProcRender> {
		private:
			friend class util::Singleton<PostProcRender>;

		private:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			PostProcRender() = default;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			virtual ~PostProcRender();

		public:

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ポストの初期化
			 * @return
			 */
			void Initialize();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ポストのリセット
			 * @return
			 */
			void Reset();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	描画オブジェクトの登録
			 * @param	pProc プロセス
			 * @return
			 */
			void Register(obj::Proc* pProc);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ポストの更新
			 * @return
			 */
			void Update();

		private:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	ポスト処理コマンド設定
			 *			別スレッドから呼ばれている事に注意
			 * @return
			 */
			virtual void OnDraw(ID3D11DeviceContext* pContext) override final;

		private:
			std::vector<obj::Proc*>	m_pProcs;	///< 描画オブジェクトポインタ

		};
	}
};
