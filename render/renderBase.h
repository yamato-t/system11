﻿#pragma once

#include "../dx11/dx11.h"
#include "../obj/drawObj.h"
#include "../obj/drawObjContainer.h"

#include "../texture/renderTarget.h"
#include "../texture/depthStencil.h"

#include "../render/renderState.h"

namespace core {

	namespace render {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * 描画処理の基底クラス
		 */
		class Render
		{
		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			Render() = default;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			virtual ~Render() = default;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	描画
			 * @return
			 */
			void Draw();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	描画コマンド設定
			 * @param	pContext		描画コンテキスト
			 * @param	pParam		描画パラメータ
			 * @return
			 */
			void SetCommand(ID3D11DeviceContext* pContext, void* pParam);

		private:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	描画コマンド設定
			 * @param	pContext		描画コンテキスト
			 * @return
			 */
			virtual void OnDraw(ID3D11DeviceContext* pContext) = 0;
		};
	}
};
