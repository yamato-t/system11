#pragma once

#include "../dx11/dx11def.h"

namespace core {

	namespace render {

		//---------------------------------------------------------------------------------
		/**
		 * @brief	
		 * 描画設定
		 */
		class RenderState
		{
		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	
			 * ブレンド設定の種類
			 */
			enum blendState
			{
				default,
				alphaBlend,
				add,
				sub,
				multi,
			};

			//---------------------------------------------------------------------------------
			/**
			 * @brief	
			 * 深度ステンシル設定
			 */
			struct DepthStencilState
			{
				bool		m_isZTest;			///< 深度テストを行うか否か
				bool		m_isWriteZ;			///< 深度書き込みを行うか否か

				bool		m_isStencilTest;	///< ステンシルテストを行うか否か
				uint8_t		m_stencilRead;		///< ステンシルテストの読み取り部分のマスク
				uint8_t		m_stencilWrite;		///< ステンシルテストを書き込み部分のマスク
				uint8_t		m_stencilRef;		///< ステンシルテスト参照値

				// 関数の指定が足りない

				DepthStencilState()
				{
					m_isZTest = m_isWriteZ = true;
					m_isStencilTest = false;
					m_stencilRead = m_stencilWrite = 0xff;
					m_stencilRef = 0;
				}
			};

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			RenderState();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			virtual ~RenderState();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ブレンドの設定
			 * @param	pContext		描画コンテキスト
			 * @param	blendState		ブレンド設定
			 * @return	
			 */
			void SetBlendState(ID3D11DeviceContext* pContext, RenderState::blendState blendState);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	深度ステンシルの設定
			 * @param	pContext		描画コンテキスト
			 * @param	depthStencil	深度ステンシル設定
			 * @return	
			 */
			void SetDepthStencil(ID3D11DeviceContext* pContext, RenderState::DepthStencilState depthStencil);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ビューポートの設定
			 * @param	pContext		描画コンテキスト
			 * @param	topLeft			ビューポート左上の位置
			 * @param	size			ビューポートサイズ
			 * @return	
			 */
			void SetViewport(ID3D11DeviceContext* pContext, const vec2& topLeft, const vec2& size);

		};
	}
};