﻿#pragma once

#include "singleton.h"
#include "renderManager.h"
#include "../obj/drawObj.h"

#include "../camera/cameraOrtho.h"

namespace core {

	namespace render {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * 2Dオブジェクトの描画処理クラス
		 */
		class Obj2DRender : public Render
						  ,	public util::Singleton<Obj2DRender> {
		private:
			friend class util::Singleton<Obj2DRender>;


		private:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			Obj2DRender();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			virtual ~Obj2DRender();

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	2Dオブジェクト描画処理の初期化
			 * @return
			 */
			void Initialize();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	2Dオブジェクト描画処理のリセット
			 * @return
			 */
			void Reset();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	2Dオブジェクト描画物の更新
			 * @return
			 */
			void Update();

		private:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	2Dオブジェクトの描画コマンド設定
			 *			別スレッドから呼ばれている事に注意
			 * @return
			 */
			virtual void OnDraw(ID3D11DeviceContext* pContext) override final;

		private:
			camera::CameraOrtho	m_cameraOrtho;		///< 2D正射影カメラ

			RenderState			m_state;			///< 描画ステート設定
		};
	}
};
