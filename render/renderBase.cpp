﻿
#include "renderBase.h"
#include "../render/renderManager.h"


namespace core {

	namespace render {

		//---------------------------------------------------------------------------------
		/**
		 * @brief	描画
		 * @return
		 */
		void Render::Draw() {
			RenderManager::Instance().SetCommand(this, nullptr);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	描画コマンド設定
		 * @param	pContext		描画コンテキスト
		 * @return
		 */
		void Render::SetCommand(ID3D11DeviceContext* pContext, void* pParam) {
			OnDraw(pContext);
		}
	}
}
