﻿#pragma once

#include "singleton.h"
#include "renderManager.h"
#include "../obj/drawObj.h"
#include "../light/light.h"

#include "../camera/camera.h"

namespace core {

	namespace render {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * 3Dオブジェクトの描画処理クラス
		 */
		class Obj3DRender : public Render
						  ,	public util::Singleton<Obj3DRender> {
		private:
			friend class util::Singleton<Obj3DRender>;

		private:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			Obj3DRender();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			virtual ~Obj3DRender();

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	3Dオブジェクト描画処理の初期化
			 * @return
			 */
			void Initialize();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	3Dオブジェクト描画処理のリセット
			 * @return
			 */
			void Reset();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	3Dオブジェクト描画物の更新
			 * @return
			 */
			void Update();

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	カラーバッファを取得する
			 * @return	現在のカラーバッファ
			 */
			const texture::RenderTarget& ColorTarget() { return m_colorTarget; }

			//---------------------------------------------------------------------------------
			/**
			 * @brief	描画設定制御の取得
			 * @return	描画設定インスタンス
			 */
			RenderState& GetRenderState() { return m_state; }

		private:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	3Dオブジェクトの描画コマンド設定
			 *			別スレッドから呼ばれている事に注意
			 * @return
			 */
			virtual void OnDraw(ID3D11DeviceContext* pContext) override final;


		private:
			texture::RenderTarget	m_colorTarget;		///< カラーレンダーターゲット

			texture::RenderTarget	m_normalTarget;		///< 法線レンダーターゲット
			texture::RenderTarget	m_shadowMapTarget;	///< シャドウマップレンダーターゲット
			texture::SamplerState	m_samplerState;		///< レンダーターゲット用サンプラ

			material::Material		m_shadowMap;		///< シャドウマップシェーダ
			camera::Camera			m_camera;			///< 3Dカメラ

			light::Light			m_dirLight;			///< 平行光源

		private:
			RenderState				m_state;			///< 描画ステート設定
			texture::DepthStencil	m_depthStencil;		///< 深度ステンシルバッファ
		};
	}
};
