﻿#pragma once

#include "../obj/drawObj.h"
#include "../texture/texture.h"
#include "../texture/samplerState.h"
#include "../material/meshMaterial.h"

#include "../render/obj3dRender.h"

namespace core {

	namespace obj3d {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * メッシュ描画オブジェクトクラス
		 */
		class Mesh : public obj::DrawObj {

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			Mesh();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			virtual ~Mesh();

		public:

			//---------------------------------------------------------------------------------
			/**
			 * @brief	メッシュの読み込み
			 * @param	filePath	ファイルパス
			 * @return
			 */
			virtual void Load(std::string filePath);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	メッシュの初期化
			 * @return
			 */
			virtual void Initialize();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	描画オブジェクトの描画処理
			 * @param	pContext	描画コンテキスト
			 * @param	pMaterial	外部指定のマテリアル
			 * @return
			 */
			virtual void Draw(ID3D11DeviceContext* pContext);


		private:
			//---------------------------------------------------------------------------------
			/**
			 * @brief		頂点情報をファイルから設定
			 * @param[out]	pDest	情報の設定先
			 * @param		str		テキスト形式の情報
			 * @return
			 */
			void SetVertexInfo(float* pDest, std::string& str);

			//---------------------------------------------------------------------------------
			/**
			 * @brief		マテリアルの読み込みと設定
			 * @param		fileName	マテリアルファイル名
			 * @return
			 */
			void LoadMaterialFromFile(const std::string& fileName);

		private:
			std::string	m_resource;			///< OBJファイルリソース（テキストファイルなので文字列で持つ）
		};
	}
};
