﻿#include "material.h"

#include <stdlib.h>
#include <locale.h>

namespace core {

	namespace material {

		using namespace dx11;

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		Material::Material()
		: m_blendState(render::RenderState::blendState::default)
		{}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		Material::~Material() {
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	ブレンド設定
		 * @param	renderState		設定する種類
		 * @return
		 */
		void Material::SetBlend(render::RenderState::blendState state) {
			m_blendState = state;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	深度テスト設定
		 * @param	isZTest			深度テストを行うか否か
		 * @param	isWriteZ		深度書き込みを行うか否か
		 * @return
		 */
		void Material::SetDepthTest(bool isZTest, bool isWriteZ) {
			m_depthStencil.m_isZTest	= isZTest;
			m_depthStencil.m_isWriteZ	= isWriteZ;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	ステンシルテスト設定
		 * @param	isEnable		ステンシルテストを行うか否か
		 * @param	read			テストに使用するチェック値
		 * @param	write			テストをパスした際の書き込み値
		 * @return
		 */
		void Material::SetStencilState(bool isEnable, uint8_t read, uint8_t write) {
			m_depthStencil.m_isStencilTest		= isEnable;
			m_depthStencil.m_stencilRead		= read;
			m_depthStencil.m_stencilWrite		= write;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	シェーダの設定
		 * @param	path		シェーダファイルのパス
		 * @param	formats		頂点バッファフォーマット
		 * @return
		 */
		void Material::SetShader(const std::string& path, const std::vector<buffer::VertexBuffer::format>& formats) {
			m_shader.Create(path, formats);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	シェーダの設定
		 * @param	path		シェーダファイルのパス
		 * @param	formats		頂点バッファフォーマット
		 * @param	instanceFormats	インスタンシング用追加頂点バッファのフォーマット
		 * @return
		 */
		void Material::SetShader(const std::string& path, const std::vector<buffer::VertexBuffer::format>& formats, const std::vector<buffer::VertexBuffer::format>& instanceFormats) {
			m_shader.Create(path, formats, instanceFormats);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	ブレンド設定の取得
		 * @return	ブレンド設定
		 */
		render::RenderState::blendState Material::GetBlendState() {
			return m_blendState;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	深度ステンシル設定の取得
		 * @return	深度ステンシル設定
		 */
		const render::RenderState::DepthStencilState& Material::GetDepthStencil() {
			return m_depthStencil;
		}
	};
};
