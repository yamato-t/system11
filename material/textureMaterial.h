﻿#pragma once

#include "../material/material.h"
#include "../texture/texture.h"
#include "../render/obj3dRender.h"

namespace core {

	namespace material {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * マテリアル生成情報
		 */
		struct CreateTextureMaterialData {
			std::string		texturePath;		///< テクスチャパス
		};

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * マテリアル情報クラス
		 */
		class TextureMaterial : public Material {
		public:
			texture::Texture*		m_pTextures;			///< テクスチャ
			int						m_textureCount;			///< テクスチャ数

			TextureMaterial(){}

			virtual ~TextureMaterial(){
				if (m_pTextures) {
					delete[] m_pTextures;
					m_pTextures = nullptr;
				}
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	マテリアルデータの設定
			 * @param	pData		元となるデータのポインタ
			 * @return
			*/
			virtual void TextureMaterial::SetData(void* pData) override {
				auto p = (CreateTextureMaterialData*)(pData);

				m_pTextures = new texture::Texture[1];
				m_pTextures[0].Create(p->texturePath);
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	マテリアルのコマンド設定
			 * @param	pContext	描画コンテキスト
			 * @return
			*/
			virtual void TextureMaterial::SetCommand(ID3D11DeviceContext* pContext) override {

				// シェーダー設定
				pContext->IASetInputLayout(m_shader.VertexLayout());
				pContext->VSSetShader(m_shader.VertexShader(), nullptr, 0);
				pContext->PSSetShader(m_shader.PixelShader(), nullptr, 0);

				if (m_pTextures)
				{
					// テクスチャ設定
					IShaderResourceView*	pRes[1] = { m_pTextures[0].GetTextureView() };
					pContext->PSSetShaderResources(0, 1, pRes);
				}
			}
		};
	}
};
