﻿#pragma once

#include "../dx11/dx11.h"
#include "../math/math.h"

#include "../shader/shader.h"

#include "../buffer/vertexBuffer.h"
#include "../render/renderState.h"

namespace core
{
	namespace material
	{
		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * マテリアル情報クラス
		 */
		class Material {
		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			Material();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			virtual ~Material();

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	ブレンド設定
			 * @param	renderState		設定する種類
			 * @return
			 */
			void SetBlend(render::RenderState::blendState state);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	深度テスト設定
			 * @param	isZTest			深度テストを行うか否か
			 * @param	isWriteZ		深度書き込みを行うか否か
			 * @return
			 */
			void SetDepthTest(bool isZTest, bool isWriteZ);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ステンシルテスト設定
			 * @param	isEnable		ステンシルテストを行うか否か
			 * @param	read			テストに使用するチェック値
			 * @param	write			テストをパスした際の書き込み値
			 * @return
			 */
			void SetStencilState(bool isEnable, uint8_t read, uint8_t write);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	シェーダの設定
			 * @param	path		シェーダファイルのパス
			 * @param	formats		頂点バッファフォーマット
			 * @return
			 */
			void SetShader(const std::string& path, const std::vector<buffer::VertexBuffer::format>& formats);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	シェーダの設定
			 * @param	path		シェーダファイルのパス
			 * @param	formats		頂点バッファフォーマット
			 * @param	instanceFormats	インスタンシング用追加頂点バッファのフォーマット
			 * @return
			 */
			void SetShader(const std::string& path, const std::vector<buffer::VertexBuffer::format>& formats, const std::vector<buffer::VertexBuffer::format>& instanceFormats);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ブレンド設定の取得
			 * @return	ブレンド設定
			 */
			render::RenderState::blendState GetBlendState();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	深度ステンシル設定の取得
			 * @return	深度ステンシル設定
			 */
			const render::RenderState::DepthStencilState& GetDepthStencil();


			//---------------------------------------------------------------------------------
			/**
			 * @brief	マテリアルデータの設定
			 * @param	pData		元となるデータのポインタ
			 * @return
			 */
			virtual void SetData(void* pData) {};

			//---------------------------------------------------------------------------------
			/**
			 * @brief	マテリアルのコマンド設定
			 * @param	pContext	描画コンテキスト
			 * @return
			 */
			virtual void SetCommand(ID3D11DeviceContext* pContext)
			{
				// シェーダー設定
				pContext->IASetInputLayout(m_shader.VertexLayout());
				pContext->VSSetShader(m_shader.VertexShader(), nullptr, 0);
				pContext->PSSetShader(m_shader.PixelShader(), nullptr, 0);
			}


		protected:
			render::RenderState::DepthStencilState	m_depthStencil;		///< 深度ステンシル設定
			render::RenderState::blendState			m_blendState;		///< ブレンド設定
			shader::Shader							m_shader;			///< シェーダ
		};
	}
};
