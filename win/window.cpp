#include "Window.h"
#include "thread.h"
#include "../input/input.h"
	
namespace core {

	namespace windows {

		//---------------------------------------------------------------------------------
		/**
		 * @brief	ウィンドウスレッドのワーク関数
		 * @param	pParameter	インスタンスハンドル
		 * @return
		 */
		uint32_t WINAPI WindowWorker(void* pParameter) {
			Window::Instance().Worker((HINSTANCE*)pParameter);
			return 0;

		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	ウィンドウプロシージャのコールバック関数
		 * @return
		 */
		LRESULT CALLBACK WndProc(HWND windouHandle, UINT msg, WPARAM wParam, LPARAM lParam) {
			return Window::Instance().MsgProc(windouHandle, msg, wParam, lParam);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	
		 * ウィンドウ制御インプリメントクラス
		 */
		class Window::cImpl {
		public:

			/**
			 * @brief	ウィンドウスレッドのステータス列挙
			 */
			enum {
				INITIALIZE,
				WORK,
				END,
			};

		public:
			HWND 				m_windowHandle;				///< ウィンドウハンドル
			HANDLE              m_createdEvent;				///< 生成イベントシグナル
			uint8_t				m_status;					///< 現在のステータス
			byte				m_keyState[256];			///< キー入力情報
			util::Thread		m_thread;					///< ウィンドウ制御スレッド


		public:
			cImpl()
				:m_windowHandle(0)
				, m_status(INITIALIZE) {
				m_createdEvent = CreateEvent(nullptr, true, false, "CREATE_WINDOW");
			}
			~cImpl() {}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ウィンドウスレッドの生成
			 * @param	hInstance	インスタンスハンドル
			 * @return	生成の成否
			 */
			HRESULT Create(HINSTANCE hInstance) {

				m_thread.Start(WindowWorker, &hInstance);

				return S_OK;
			}


			//---------------------------------------------------------------------------------
			/**
			 * @brief	ウィンドウの生成
			 * @param	hInstance	インスタンスハンドル
			 * @param	width		横幅
			 * @param	height		縦幅
			 * @param	name		ウィンドウ名
			 * @return	生成の成否
			 */
			HRESULT Initialize(HINSTANCE hInstance, uint32_t width, uint32_t height, char* name) {
				// ウィンドウの定義
				WNDCLASSEX  wc;
				ZeroMemory(&wc, sizeof(wc));
				wc.cbSize			= sizeof(wc);
				wc.style			= (CS_HREDRAW | CS_VREDRAW);
				wc.lpfnWndProc		= WndProc;
				wc.hInstance		= hInstance;
				wc.hIcon			= LoadIcon(NULL, IDI_APPLICATION);
				wc.hCursor			= LoadCursor(NULL, IDC_ARROW);
				wc.hbrBackground	= (HBRUSH)GetStockObject(LTGRAY_BRUSH);
				wc.lpszClassName	= name;
				wc.hIconSm			= LoadIcon(NULL, IDI_APPLICATION);
				RegisterClassEx(&wc);

				//ウィンドウの作成
				m_windowHandle = CreateWindow(name, name, WS_OVERLAPPEDWINDOW, 0, 0, width, height, 0, 0, hInstance, 0);
				if (!m_windowHandle) { return E_FAIL; }

				//ウインドウの表示
				ShowWindow(m_windowHandle, SW_SHOW);
				UpdateWindow(m_windowHandle);

				// ウィンドウ作成終了イベントシグナルを設定
				SetEvent(m_createdEvent);

				m_status = WORK;

				return S_OK;
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	スレッドワーカー
			 * @param	pInstance	インスタンスハンドルのポインタ
			 * @return	
			 */
			void Worker(HINSTANCE* pInstance) {
				// メッセージループ
				MSG msg = { 0 };
				ZeroMemory(&msg, sizeof(msg));

				while (msg.message != WM_QUIT) {
					if (cImpl::INITIALIZE == m_status) {
						Initialize(*pInstance, WINDOW_WIDTH, WINDOW_HEIGHT, APP_NAME);
					}
					else if (cImpl::WORK == m_status) {
						if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
							TranslateMessage(&msg);
							DispatchMessage(&msg);
						}

						GetKeyboardState(m_keyState);
						if(input::Input::IsValid()) { input::Input::Instance().UpdateKeyState(m_keyState); }

					}
				}

				m_status = END;

				// スレッド終了
				//_endthread();
			}


			//---------------------------------------------------------------------------------
			/**
			 * @brief	ハンドルの取得
			 * @return	ウィンドウハンドル
			 */
			const HWND&	Handle() const {
				return m_windowHandle;
			}
		};


		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		Window::Window() {
			m_pImpl.reset(new Window::cImpl());
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		Window::~Window() {
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	ウィンドウプロシージャ
		 */
		LRESULT Window::MsgProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
			switch (uMsg) {
			case WM_KEYDOWN:
				switch ((char)wParam) {
				case VK_ESCAPE://ESCキーで
					PostQuitMessage(0);
					break;
				}
				break;
			case WM_DESTROY:
				PostQuitMessage(0);
				break;
			}
			return DefWindowProc(hWnd, uMsg, wParam, lParam);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	ウィンドウスレッドの生成
		 * @param	hInstance	インスタンスハンドル
		 * @return	生成の成否
		 */
		HRESULT Window::Create(HINSTANCE hInstance) {
			return m_pImpl->Create(hInstance);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	スレッドワーカー
		 * @param	pInstance	インスタンスハンドルのポインタ
		 * @return	
		 */
		void Window::Worker(HINSTANCE* pInstance) {
			m_pImpl->Worker(pInstance);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	ウィンドウ生成終了までの処理待機
		 * @return	
		 */
		void Window::Wait() {
			// SetEventまで待機
			WaitForSingleObject(m_pImpl->m_createdEvent, INFINITE);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	ウィンドウハンドルの取得
		 * @return	ウィンドウハンドル
		 */
		const HWND&	Window::Handle() {
			return m_pImpl->Handle();
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	ウィンドウが終了しているか否かの取得
		 * @return	終了していればtrue
		 */
		bool Window::IsEnd() {
			return (m_pImpl->m_status == cImpl::END);
		}
	}
}