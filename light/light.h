#pragma once

#include "../obj/objBase.h"
#include "../math/math.h"
#include "../buffer/constantBuffer.h"

namespace core {

	namespace light {

		//---------------------------------------------------------------------------------
		/**
		 * @brief	
		 * 平行光源の制御
		 */
		class Light : public obj::ObjBase {

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コントラクタ
			 */
			Light();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			virtual ~Light();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ライト向き設定
			 * @param	dir	ワールド空間上の向き
			 * @return
			 */
			void Dir(const vec3& dir);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ライト向き取得
			 * @return　ワールド空間上の向き
			 */
			const vec3& Dir() { return m_dir; }

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ライトカラー設定
			 * @param	color	ライト色
			 * @return
			 */
			void Color(const vec4& color);


			//---------------------------------------------------------------------------------
			/**
			 * @brief	透視投影カメラのコンスタントバッファを更新
			 * @param	pContext	描画コンテキスト
			 * @return
			 */
			virtual void UpdateConstantBuffer(ID3D11DeviceContext* pContext);

		protected:
			buffer::ConstantBuffer	m_constantBuffer;		///< カメラコンスタントバッファ

			vec3					m_dir;					///< ターゲットのワールド座標
			vec4					m_color;				///< ライトカラー
		};
	}
};