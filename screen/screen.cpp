﻿#include "screen.h"
#include "../render/obj2dRender.h"
#include "../win/window.h"
#include "../material/textureMaterial.h"

namespace core {

	namespace objScreen {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * スクリーン描画オブジェクトのコンスタントバッファ内容
		 */
		struct ConstantBufferFormat {
			matrix world;		///< ワールド変換行列
		};

		// 頂点バッファの内容を作成
		struct IBuffer {
			vec3		pos;		// 位置
			vec2		uv;			// UV
		};

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		Screen::Screen()
		: m_currentVertexBufferIndex(0) {
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		Screen::~Screen() {
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	描画リセット
		 * @return
		 */
		void Screen::Reset() {
			m_currentVertexBufferIndex = 0;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	スクリーンの初期化
		 * @return
		 */
		void Screen::Initialize() {

			// 頂点バッファのフォーマットを定義
			uint32_t formats[] = {
				buffer::VertexBuffer::format::pos,
				buffer::VertexBuffer::format::uv,
			};

			IBuffer vertexBuffers[4];
			{
				vertexBuffers[0].pos	= vec3(0.0f, 0.0f, 0);
				vertexBuffers[0].uv		= vec2(0, 0);

				vertexBuffers[1].pos	= vec3(0.0f, WINDOW_HEIGHT, 0);
				vertexBuffers[1].uv		= vec2(0, 1);

				vertexBuffers[2].pos	= vec3(WINDOW_WIDTH, 0.0f, 0);
				vertexBuffers[2].uv		= vec2(1, 0);

				vertexBuffers[3].pos	= vec3(WINDOW_WIDTH, WINDOW_HEIGHT, 0);
				vertexBuffers[3].uv		= vec2(1, 1);
			}

			uint32_t formatCount = sizeof(formats) / sizeof(formats[0]);
			uint32_t vertexCount = sizeof(vertexBuffers) / sizeof(vertexBuffers[0]);

			for(int i = 0; i < 5; i++)
			{
				m_testVertexBuffer[i].Create(formatCount, formats, vertexCount, vertexBuffers);
			}

			// インデックスバッファを作成
			m_pIndexBuffers = new buffer::IndexBuffer[1];
			uint16_t index[] = { 0, 1, 2, 3 };
			m_pIndexBuffers[0].Create(sizeof(uint16_t), vertexCount, index);


			// マテリアル作成
			m_materialNum	= 1;
			m_pMaterials	= new material::Material*[m_materialNum];
			m_pMaterials[0]	= new material::TextureMaterial();
			// シェーダ設定
			std::string shaderPath = "resource/hlsl/vertexTexture.hlsl";
			m_pMaterials[0]->SetShader(shaderPath, m_testVertexBuffer[0].GetFormat());

			// サンプラ
			m_samplerState.Create(D3D11_TEXTURE_ADDRESS_WRAP, D3D11_FILTER_MIN_MAG_MIP_LINEAR);

			// コンスタントバッファ作成
			m_constantBuffer.Create(sizeof(ConstantBufferFormat));

			// スクリーン投影設定に更新
			m_cameraOrtho.UpdateScreenViewProjection();
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	スクリーンの描画
		 * @param	pContext	描画コンテキスト
		 * @param	pTexture　	描画するテクスチャのポインタ
		 * @param	pRect　		描画矩形（xy：左上 zw：右下）
		 * @return
		 */
		void Screen::DrawTexture(ID3D11DeviceContext* pContext, texture::Texture* pTexture, vec4* pRect) {
			DrawShaderResource(pContext, pTexture->GetTextureView(), pRect);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	スクリーンの描画(シェーダリソースビュー指定)
		 * @param	pContext	描画コンテキスト
		 * @param	pTexture　	描画するシェーダリソース
		 * @param	pRect　		描画矩形（xy：左上 zw：右下）
		 * @return
		 */
		void Screen::DrawShaderResource(ID3D11DeviceContext* pContext, IShaderResourceView* pShaderResouceView, vec4* pRect)
		{
			// 更新がある時のみ
			if (m_update) {
				// コンスタントバッファ
				ConstantBufferFormat cBuffer;
				// ワールド変換行列
				math::MatrixIdentity(&cBuffer.world);

				// コンスタントバッファへコピー
				D3D11_MAPPED_SUBRESOURCE	pData;
				ID3D11Buffer*				pBuffer = m_constantBuffer.GetBuffer();

				if (SUCCEEDED(pContext->Map(pBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &pData))) {
					memcpy(pData.pData, (void*)&cBuffer, sizeof(ConstantBufferFormat));
					pContext->Unmap(pBuffer, 0);
				}

				m_update = false;
			}

			// スクリーンカメラコンスタントバッファ更新
			m_cameraOrtho.UpdateConstantBuffer(pContext);

			auto vertex = &m_testVertexBuffer[m_currentVertexBufferIndex++];
			if (pRect) {
				IBuffer vertexBuffers[4]; {
					vertexBuffers[0].pos = vec3(pRect->x, pRect->y, 0);
					vertexBuffers[0].uv = vec2(0, 0);

					vertexBuffers[1].pos = vec3(pRect->x, pRect->w, 0);
					vertexBuffers[1].uv = vec2(0, 1);

					vertexBuffers[2].pos = vec3(pRect->z, pRect->y, 0);
					vertexBuffers[2].uv = vec2(1, 0);

					vertexBuffers[3].pos = vec3(pRect->z, pRect->w, 0);
					vertexBuffers[3].uv = vec2(1, 1);
				}
				// 頂点バッファ更新
				// 毎フレーム更新するのは無駄
				vertex->Update(pContext, vertexBuffers, sizeof(vertexBuffers));
			}

			// シェーダーにコンスタントバッファデータを渡す
			ID3D11Buffer* p[1] = { m_constantBuffer.GetBuffer() };
			pContext->VSSetConstantBuffers(0, 1, p);
			pContext->PSSetConstantBuffers(0, 1, p);


			// 頂点バッファ設定
			auto buffer = vertex->GetBuffer();
			UINT stride = vertex->Stride();
			UINT offset = vertex->Offset();
			pContext->IASetVertexBuffers(0, 1, &buffer, &stride, &offset);

			// プリミティブトポロジー設定
			pContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

			// インデックスバッファ設定
			auto index = GetIndexBuffer();
			auto indexBuffer = index->GetBuffer();
			pContext->IASetIndexBuffer(indexBuffer, index->GetDXGIFormat(), 0);

			// マテリアル設定
			m_pMaterials[0]->SetCommand(pContext);

			// 外部指定テクスチャ設定
			IShaderResourceView*	pRes[1] = { pShaderResouceView };
			pContext->PSSetShaderResources(0, 1, pRes);

			// サンプラーステート設定
			ID3D11SamplerState*			pSampler[1] = { m_samplerState.GetSamplerState() };
			pContext->PSSetSamplers(0, 1, pSampler);

			// インデックスバッファを利用した描画
			pContext->DrawIndexed(index->GetCount(), 0, 0);

		}

	}
}
