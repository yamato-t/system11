﻿#include "rayMarchingHeightMap.h"
#include "../render/obj2dRender.h"
#include "../win/window.h"

namespace core {

	namespace objScreen {

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		RayMarchingHeightMap::RayMarchingHeightMap() {
			// 描画登録
			obj::DrawObjContainer::Instance().Register(obj::DrawObjContainer::layer::ui, this);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		RayMarchingHeightMap::~RayMarchingHeightMap() {
			// 登録抹消
			//render::Obj2DRender::Instance().Erase(this);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	スプライトの初期化
		 * @return
		 */
		void RayMarchingHeightMap::Initialize() {

			// 頂点バッファのフォーマットを定義
			uint32_t formats[] = {
				buffer::VertexBuffer::format::pos,
			};

			// 頂点バッファの内容を作成
			struct IBuffer {
				vec3		pos;		// 位置
			};

			// ピクセルに合わせた頂点設定なので、Yの上下が反転する事を考慮
			IBuffer vertexBuffers[4];
			{
				vertexBuffers[0].pos = vec3(0.0f, 0.0f, 0);
				vertexBuffers[1].pos = vec3(0.0f, WINDOW_HEIGHT, 0);
				vertexBuffers[2].pos = vec3(WINDOW_WIDTH, 0.0f, 0);
				vertexBuffers[3].pos = vec3(WINDOW_WIDTH, WINDOW_HEIGHT, 0);
			}
			uint32_t formatCount = sizeof(formats) / sizeof(formats[0]);
			uint32_t vertexCount = sizeof(vertexBuffers) / sizeof(vertexBuffers[0]);
			m_vertexBuffer.Create(formatCount, formats, vertexCount, vertexBuffers);

			// インデックスバッファを作成
			m_pIndexBuffers = new buffer::IndexBuffer[1];
			uint16_t index[] = { 0, 1, 2, 3 };
			m_pIndexBuffers[0].Create(sizeof(uint16_t), vertexCount, index);

			// マテリアル作成
			m_materialNum	= 1;
			m_pMaterials	= new material::Material*[m_materialNum];
			m_pMaterials[0]	= new material::Material();
			// シェーダ設定
			std::string shaderPath = "resource/hlsl/rayMarchingHeightMap.hlsl";
			m_pMaterials[0]->SetShader(shaderPath, m_vertexBuffer.GetFormat());

			// 高さマップ
			m_heightMap.Create("resource/texture/test.jpg");

			// サンプラ
			m_samplerState.Create(D3D11_TEXTURE_ADDRESS_WRAP, D3D11_FILTER_MIN_MAG_MIP_LINEAR);

			m_prepared = true;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	スプライトの描画
		 * @param	pContext	描画コンテキスト
		 * @param	pMaterial	外部指定のマテリアル
		 * @return
		 */
		void RayMarchingHeightMap::Draw(ID3D11DeviceContext* pContext) {
			// 頂点バッファ設定
			auto vertex = GetVertexBuffer();
			auto buffer = vertex->GetBuffer();
			UINT stride = vertex->Stride();
			UINT offset = vertex->Offset();
			pContext->IASetVertexBuffers(0, 1, &buffer, &stride, &offset);

			// プリミティブトポロジー設定
			pContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

			// インデックスバッファ設定
			auto index			= GetIndexBuffer();
			auto indexBuffer	= index->GetBuffer();
			pContext->IASetIndexBuffer(indexBuffer, index->GetDXGIFormat(), 0);

			// マテリアル設定
			m_pMaterials[0]->SetCommand(pContext);

			// 高さマップ設定
			IShaderResourceView*	pRes[1] = { m_heightMap.GetTextureView() };
			pContext->PSSetShaderResources(0, 1, pRes);

			// サンプラーステート設定
			ID3D11SamplerState*			pSampler[1] = { m_samplerState.GetSamplerState() };
			pContext->PSSetSamplers(0, 1, pSampler);

			// インデックスバッファを利用した描画
			pContext->DrawIndexed(index->GetCount(), 0, 0);
		}
	}
}
