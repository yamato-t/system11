﻿#include "constantBuffer.h"
#include "../dx11/dx11.h"
#include "../math/math.h"

namespace core {

	namespace buffer {

		using namespace dx11;

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		ConstantBuffer::ConstantBuffer() {
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		ConstantBuffer::~ConstantBuffer() {
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンスタントバッファ作成
		 * @param	size	コンスタントバッファのサイズ
		 * @return
		 */
		void ConstantBuffer::Create(uint32_t size) {
			IBuffer::Create(D3D11_BIND_CONSTANT_BUFFER, size, 1, D3D11_USAGE_DYNAMIC, D3D11_CPU_ACCESS_WRITE);
		}
	}
}

