#pragma once

#include "../dx11/dx11def.h"
#include "buffer.h"
#include <string>

namespace core {

	namespace buffer {

		//---------------------------------------------------------------------------------
		/**
		 * @brief	
		 * インデックスバッファ
		 */
		class IndexBuffer : public IBuffer 
		{
	
		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			IndexBuffer();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			virtual ~IndexBuffer();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	インデックスバッファの作成
			 * @param	bufferStride	インデックスバッファを構築するデータのサイズ
			 * @param	indexCount		インデックスの数
			 * @param	pResource		インデックスバッファを構成するデータ配列のポインタ
			 * @return
			 */
			void Create(uint32_t bufferStride, uint32_t indexCount, void* pResource);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	インデックスバッファのフォーマットを取得
			 * @return　インデックスバッファのフォーマット
			 */
			const DXGI_FORMAT GetDXGIFormat();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	インデックスバッファ数を取得
			 * @return　インデックスバッファの数
			 */
			const uint32_t GetCount();

		private:
			uint32_t m_count;		///< インデックスバッファの数
		};
	}

}