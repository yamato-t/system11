﻿#pragma once

#include "../dx11/dx11def.h"
#include "../math/math.h"

namespace core {

	namespace buffer {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * D3Dデバイスのバッファ基底クラス
		 */
		class IBuffer {

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			IBuffer(){}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			virtual ~IBuffer(){}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	バッファの作成
			 * @param	bindFlags		バインドフラグ
			 * @param	stride			構造化バッファサイズ
			 * @param	count			構造化バッファ数
			 * @param	usage			バッファの利用方法種類の設定
			 * @param	cpuAccessFlag	CPUアクセス可能か否か
			 * @param	miscFlags		その他属性フラグ
			 * @param	pInit			初期データ
			 * @return
			 */
			void Create(uint32_t	bindFlags,
						uint32_t	stride,
						uint32_t	count,
						D3D11_USAGE usage = D3D11_USAGE_DEFAULT,
						uint32_t	cpuAccessFlag = 0,
						uint32_t	miscFlags = 0,
						D3D11_SUBRESOURCE_DATA* pInit = nullptr);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	バッファの更新
			 * @param	pContext	描画コンテキスト
			 * @param	data　		更新データ
			 * @param	dataSize　	データサイズ
			 * @return
			 */
			void IBuffer::Update(ID3D11DeviceContext* pContext, void* data, uint32_t dataSize);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	バッファの取得
			 * @return	バッファ
			 */
			ID3D11Buffer* GetBuffer();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	バッファの全体のサイズの取得
			 * @return	バッファ全体のサイズ
			 */
			const uint32_t Size();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	バッファサイズの取得
			 * @return	バッファサイズ
			 */
			const uint32_t Stride();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	バッファ内の実データまでのオフセット取得
			 * @return	バッファ内の実データまでのオフセット
			 */
			const uint32_t Offset();

		protected:
			uint32_t					m_size;			///< バッファ全体のサイズ
			uint32_t					m_stride;		///< 一バッファサイズ
			uint32_t					m_offset;		///< バッファ内の実データまでのオフセット


		private:
			dx11::ComPtr<ID3D11Buffer> m_pBuffer;		///< バッファ
		};
	}
}
