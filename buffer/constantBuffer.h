#pragma once

#include "../dx11/dx11def.h"
#include "../math/math.h"
#include "buffer.h"

namespace core {

	namespace buffer {

		//---------------------------------------------------------------------------------
		/**
		 * @brief	
		 * コンスタントバッファ
		 */
		class ConstantBuffer : public IBuffer 
		{
		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			ConstantBuffer();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			virtual ~ConstantBuffer();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンスタントバッファ作成
			 * @param	size	コンスタントバッファのサイズ
			 * @return 
			 */
			void Create(uint32_t size);
		};
	}
}