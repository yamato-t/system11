﻿#include "vertexBuffer.h"
#include "../dx11/dx11.h"
#include "../math/math.h"

namespace core {
	namespace buffer {

		using namespace math;

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		VertexBuffer::VertexBuffer() {
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		VertexBuffer::~VertexBuffer() {
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	頂点バッファの作成
		 * @param	formatCount		頂点を構成するデータのフォーマットの数
		 * @param	formats			頂点を構成するデータのフォーマット配列のポインタ
		 * @param	formats			頂点数
		 * @param	formats			頂点を構成するデータ配列のポインタ
		 * @return
		 */
		void VertexBuffer::Create(uint32_t formatCount, uint32_t* formats, uint32_t vertexCount, void* vertexBuffers) {
			uint32_t bufferStride = 0;

			for(uint32_t i = 0; i < formatCount; i++) {
				bufferStride += GetFormatSize((format)formats[i]);
				m_formats.push_back((format)formats[i]);
			}

			m_stride	= bufferStride;
			m_size		= vertexCount * m_stride;

			D3D11_SUBRESOURCE_DATA* pInitData = nullptr;
			if(vertexBuffers) {
				pInitData = new D3D11_SUBRESOURCE_DATA;
				pInitData->pSysMem			= vertexBuffers;
				pInitData->SysMemPitch		= 0;
				pInitData->SysMemSlicePitch	= 0;
			}
			IBuffer::Create(D3D11_BIND_VERTEX_BUFFER, m_size, 1, D3D11_USAGE_DYNAMIC, D3D11_CPU_ACCESS_WRITE, 0, pInitData);

			if(pInitData) {delete pInitData; }
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	頂点を構成するデータのフォーマットを取得
		 * @return　フォーマットが入ったコンテナ
		 */
		const std::vector<VertexBuffer::format>& VertexBuffer::GetFormat() const {
			return m_formats;
		}


		//---------------------------------------------------------------------------------
		/**
		 * @brief	フォーマットサイズの取得
		 * @return　フォーマットサイズ
		 */
		uint32_t VertexBuffer::GetFormatSize(format f) {

			static uint32_t formatSize[format::max]
			{
				sizeof(vec3),
				sizeof(vec4),
				sizeof(vec2),
				sizeof(vec3),
				sizeof(vec3),
				sizeof(vec3),
				sizeof(vec3),
				sizeof(vec4) * 4,
			};

			return formatSize[f];
		}
	}
}

