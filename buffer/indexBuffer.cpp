﻿#include "indexBuffer.h"
#include "../dx11/dx11.h"
#include "../math/math.h"

namespace core {
	namespace buffer {

		using namespace math;

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		IndexBuffer::IndexBuffer() {
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		IndexBuffer::~IndexBuffer() {
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	インデックスバッファの作成
		 * @param	bufferStride	インデックスバッファを構築するデータのサイズ
		 * @param	indexCount		インデックスの数
		 * @param	pResource		インデックスバッファを構成するデータ配列のポインタ
		 * @return
		 */
		void IndexBuffer::Create(uint32_t bufferStride, uint32_t indexCount, void* pResource) {

			m_stride	= bufferStride;
			m_size		= m_stride * indexCount;
			m_count		= indexCount;

			D3D11_SUBRESOURCE_DATA InitData;
			InitData.pSysMem			= pResource;
			InitData.SysMemPitch		= 0;
			InitData.SysMemSlicePitch	= 0;

			IBuffer::Create(D3D11_BIND_INDEX_BUFFER, m_size, 1, D3D11_USAGE_DEFAULT, 0, 0, &InitData);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	インデックスバッファのフォーマットを取得
		 * @return　インデックスバッファのフォーマット
		 */
		const DXGI_FORMAT IndexBuffer::GetDXGIFormat() {
			return m_stride == sizeof(uint16_t) ? DXGI_FORMAT_R16_UINT : DXGI_FORMAT_R32_UINT;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	インデックスバッファ数を取得
		 * @return　インデックスバッファの数
		 */
		const uint32_t IndexBuffer::GetCount() {
			return m_count;
		}
	}
}

