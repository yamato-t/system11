﻿#pragma once

#include "../dx11/dx11def.h"
#include <string>

namespace core {

	namespace texture {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * テクスチャの制御
		 */
		class Texture {

		private:
			class cImpl;
			std::shared_ptr<cImpl> m_pImpl;	///< インプリメントクラスポインタ

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			Texture();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~Texture();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	テクスチャ作成
			 * @param	width		横幅
			 * @param	height		縦幅
			 * @param	format		テクスチャフォーマット
			 * @param	bindFlags	バインドフラグ
			 * @return
			 */
			void Create(float width, float height, DXGI_FORMAT format = DXGI_FORMAT_R8G8B8A8_UNORM, uint32_t bindFlags = 0);


			//---------------------------------------------------------------------------------
			/**
			 * @brief	テクスチャ作成
			 * @param	filePath	ファイルパス
			 * @return
			 */
			void Create(std::string filePath);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	テクスチャ取得
			 * @return	テクスチャ
			 */
			ITexture2D* Get();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	テクスチャ取得
			 * @return	テクスチャのビュー
			 */
			IShaderResourceView*	GetTextureView();
		};
	}

}
