#pragma once

#include "../dx11/dx11def.h"
#include "texture.h"

namespace core {

	namespace texture {

		//---------------------------------------------------------------------------------
		/**
		 * @brief	
		 * 深度ステンシルの制御
		 */
		class DepthStencil {

		private:
			class cImpl;
			std::shared_ptr<cImpl> m_pImpl;	///< インプリメントクラスポインタ

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			DepthStencil();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~DepthStencil();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	深度ステンシル作成
			 * @param	width		横幅
			 * @param	height		縦幅
			 * @return
			 */
			void Create(float width, float height);


			//---------------------------------------------------------------------------------
			/**
			 * @brief	2Dバッファ取得
			 * @return	深度ステンシルのテクスチャ
			 */
			Texture*			GetTexture();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ビューの取得
			 * @return	深度ステンシルのビュー
			 */
			IDepthStencilView*	GetDepthStencilView();
		};
	}
}