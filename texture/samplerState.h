#pragma once

#include "../dx11/dx11def.h"

namespace core {

	namespace texture {

		//---------------------------------------------------------------------------------
		/**
		 * @brief	
		 * サンプラーステート制御クラス
		 */
		class SamplerState {

		private:
			class cImpl;
			std::shared_ptr<cImpl> m_pImpl;	///< インプリメントクラスポインタ

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			SamplerState();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~SamplerState();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	サンプラーステート作成
			 * @param	wrapMode	ラップモード
			 * @param	filterMode	フィルタモード
			 * @return
			 */
			void Create(D3D11_TEXTURE_ADDRESS_MODE wrapMode, D3D11_FILTER filterMode);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	サンプラーステート取得
			 * @return	サンプラーステート
			 */
			ID3D11SamplerState* GetSamplerState();
		};
	}
}