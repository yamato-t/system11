﻿#pragma once

#include "../dx11/dx11.h"
#include "../buffer/vertexBuffer.h"
#include "../buffer/indexBuffer.h"
#include "../buffer/constantBuffer.h"
#include "../material/material.h"
#include "../shader/shader.h"
#include "../texture/texture.h"
#include "../texture/samplerState.h"

#include "objBase.h"

namespace core {

	namespace obj {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * 描画オブジェクトの基底クラス
		 */
		class DrawObj : public ObjBase {

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			DrawObj();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			virtual ~DrawObj();

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	描画オブジェクトの初期化
			 * @return
			 */
			virtual void Initialize() = 0;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	描画オブジェクトの描画処理
			 * @param	pContext	描画コンテキスト
			 * @param	pMaterial	外部指定のマテリアル
			 * @return
			 */
			virtual void Draw(ID3D11DeviceContext* pContext) = 0;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	描画オブジェクトの描画処理
			 * @param	pContext	描画コンテキスト
			 * @param	pMaterial	外部指定のマテリアル
			 * @return
			 */
			void SetMaterial(material::Material* material) { m_pMaterial = material; }

			//---------------------------------------------------------------------------------
			/**
			 * @brief	頂点バッファの取得
			 * @return	頂点バッファ
			 */
			buffer::VertexBuffer*	GetVertexBuffer();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	準備済みか否か
			 * @return	準備済みならtrue
			 */
			bool	Prepared() { return m_prepared;  }


			//---------------------------------------------------------------------------------
			/**
			 * @brief	インデックスバッファの取得
			 * @param	no	インデックスバッファのコンテナ番号
			 * @return	インデックスバッファ
			 */
			buffer::IndexBuffer*	GetIndexBuffer(uint32_t no = 0);

		protected:
			buffer::VertexBuffer					m_vertexBuffer;			///< 頂点バッファ
			buffer::IndexBuffer*					m_pIndexBuffers;		///< インデックスバッファ
			buffer::ConstantBuffer					m_constantBuffer;		///< コンスタントバッファ

			material::Material**					m_pMaterials;			///< マテリアルポインタ配列
			uint16_t								m_materialNum;			///< マテリアル数
			texture::SamplerState					m_samplerState;			///< サンプラ

			material::Material*						m_pMaterial = nullptr;	///< マテリアル

			bool									m_prepared	= false;
		};
	}
};
