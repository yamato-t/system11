#pragma once

#include "singleton.h"
#include "../obj/drawObj.h"

namespace core {

	namespace obj {

		//---------------------------------------------------------------------------------
		/**
		 * @brief	
		 * 描画オブジェクトのコンテナ
		 */
		class DrawObjContainer : public util::Singleton<DrawObjContainer> {

		private:
			friend class util::Singleton<DrawObjContainer>;


		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	描画レイヤー
			 */
			enum layer {
				// 3D
				opaque,			///< 不透明
				translucent,	///< 半透明
				// 2D
				ui,				///< ユーザーインターフェイス
				// 
				debug,			///< デバッグ描画物

				max,
			};

		private:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			DrawObjContainer();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			virtual ~DrawObjContainer();

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	描画オブジェクトの登録
			 * @param	layer	 描画レイヤー
			 * @param	pDrawObj 描画オブジェクト 
			 * @return	
			 */
			void Register(layer layer, obj::DrawObj* pDrawObj);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	描画オブジェクトの描画順ソート
			 * @param	layer	 描画レイヤー
			 * @return	
			 */
			void Sort(layer layer);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	描画オブジェクトの取得
			 * @param	layer	 描画レイヤー
			 * @return	レイヤーに登録されている描画オブジェクト
			 */
			std::vector<obj::DrawObj*>& GetDrawObj(layer layer);




		private:
			std::vector<obj::DrawObj*>	m_pDrawObj[layer::max];	///< 描画オブジェクトポインタ

		};
	}
};