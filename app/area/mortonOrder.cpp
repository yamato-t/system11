﻿#include "mortonOrder.h"

namespace app {

	namespace area {
		vec4	startPoint(0);
		vec4	splitOffset(0);
		int		hierarchyMax = 0;

		//---------------------------------------------------------------------------------
		/**
		 * @brief	指定値に対するbit飛ばしを行う（2Dモートン番号用）
		 * @param	n		指定値
		 * @return
		 */
		static int BitSeparate2D(int n)
		{
			n = (n | (n << 8)) & 0x00ff00ff;
			n = (n | (n << 4)) & 0x0f0f0f0f;
			n = (n | (n << 2)) & 0x33333333;
			return	(n | (n << 1)) & 0x55555555;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	指定値に対するbit飛ばしを行う（3Dモートン番号用）
		 * @param	n		指定値
		 * @return
		 */
		static int BitSeparate3D(int n)
		{
			n = (n | (n << 8)) & 0x0000f00f;
			n = (n | (n << 4)) & 0x000c30c3;
			return	(n | (n << 2)) & 0x00249249;
		}


		//---------------------------------------------------------------------------------
		/**
		 * @brief	エリアを作成する（XZ平面の分割）
		 * @param	start		開始座標
		 * @param	size		サイズ
		 * @param	hierarchy	階層数
		 * @return
		 */
		void MortonOrder::SetUp(const vec4& start, const vec4& size, int hierarchy) {

			hierarchyMax = hierarchy - 1;

			auto splitNum = pow(2, hierarchyMax);

			auto offset	= (start + size) / (float)splitNum;
			splitOffset = offset;
			startPoint	= start;

			splitOffset.w	= 0;
			startPoint.w	= 0;

		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	モートン番号を取得する
		 * @param	point		ポイント
		 * @return	対応する最下層モートン番号
		 */
		int MortonOrder::MortonNumber(const vec4& point) {

			auto v = point - startPoint;

			auto splitX = (int)(v.x / splitOffset.x);
			auto splitY = (int)(v.y / splitOffset.y);
			auto splitZ = (int)(v.z / splitOffset.z);
			auto res = ((BitSeparate3D(splitX)) |
				(BitSeparate3D(splitY) << 1) |
				(BitSeparate3D(splitZ) << 2)
				);

#if false
			auto splitX = (int)(v.x / splitOffset.x);
			auto splitZ = (int)(v.z / splitOffset.z);
			res = (	(BitSeparate2D(splitX)) |
					(BitSeparate2D(splitZ) << 1));
#endif

			return res;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	モートン番号を取得する
		 * @param	aabb		境界ボリューム（AABB）
		 * @return	階層と階層におけるモートン番号のペア
		 */
		std::pair<int, int> MortonOrder::MortonNumber(const core::math::collision::AABB& aabb) {

			constexpr int bitnum = 3;

			auto lpos = aabb.m_vCenter - aabb.m_vDistance;
			auto rpos = aabb.m_vCenter + aabb.m_vDistance;

			auto l = MortonNumber(lpos);
			auto r = MortonNumber(rpos);
			auto temp = l ^ r;

			// 階層シフト数を計算する
			int shiftNum = 0;
			for (; shiftNum <= hierarchyMax; shiftNum++)
			{
				auto t = temp >> (shiftNum * bitnum);
				if (t == 0) { break; }
			}

			auto h	= hierarchyMax - shiftNum;
			auto no = (r >> shiftNum);

			return { h, no };
		}

	}

}
