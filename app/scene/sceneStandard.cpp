﻿#include "sceneStandard.h"

namespace app {

	namespace scene {

		//---------------------------------------------------------------------------------
		/**
		 * @brief	シーン初期化
		 * @return
		 */
		void SceneStandard::Initialize() {
			//m_instance.Initialize();
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	シーン更新
		 * @return
		 */
		void SceneStandard::Update() {

		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	シーン終了
		 * @return
		 */
		void SceneStandard::Finalize() {
		}
	}
}
