﻿#include "sceneManager.h"

namespace app {

	namespace scene {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * 描画管理のインプリメントクラス
		 */
		class SceneManager::cImpl {
		public:
			std::vector<std::shared_ptr<SceneBase>> m_scenes;
			std::weak_ptr<SceneBase>				m_current;

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			cImpl() = default;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~cImpl() {

				for (auto scene : m_scenes)
				{
					scene->Finalize();
					scene.reset();
				}
				m_scenes.clear();

				m_current.reset();

			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	現在のシーンを更新する
			 */
			void UpdateCurrentScene()
			{
				auto p = m_current.lock();
				if (p)
				{
					p->Update();
				}
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	シーンを追加する
			 */
			void AddScene(std::shared_ptr<SceneBase> scene)
			{
				scene->Initialize();
				m_scenes.push_back(scene);
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	シーンを切り替える
			 */
			void ChangeScene(std::weak_ptr<SceneBase> scene)
			{
				auto p = m_current.lock();
				if (p)
				{
					p->Finalize();
				}

				m_current = scene;
			}

		};

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		SceneManager::SceneManager() {
			m_pImpl.reset(new SceneManager::cImpl());
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		SceneManager::~SceneManager() {
			m_pImpl.reset();
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	シーンを更新する
		 */
		void SceneManager::Update()
		{
			m_pImpl->UpdateCurrentScene();
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	シーンを追加する
		 */
		void SceneManager::Add(std::shared_ptr<SceneBase> scene)
		{
			m_pImpl->AddScene(scene);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	シーンを切り替える
		 */
		void SceneManager::Change(std::weak_ptr<SceneBase> scene)
		{
			m_pImpl->ChangeScene(scene);
		}

	}
}
