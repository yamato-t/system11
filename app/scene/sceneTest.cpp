﻿#include "sceneTest.h"

#include "callback.h"
#include "crc32.h"

#include <type_traits>
#include <array>

namespace app {

	namespace scene {

		namespace test {
			// https://cpprefjp.github.io/reference/tuple/tuple/get.html

			template<class T>
			class Type
			{
			public:
				void init() { vec.clear(); }
				void func(T val) { vec.emplace_back(val); }

			private:
				std::vector<T> vec;
			};

			class Types
			{
			public:
				Types() {
					types = std::make_tuple(
						std::make_shared<Type<int>>(),
						std::make_shared<Type<float>>(),
						std::make_shared<Type<char>>()
					);
				}

				~Types() {
					init(types);
				}

				template<class T>
				void func(T val) {
					auto& type = std::get<std::shared_ptr<Type<T>>>(types);
					type->func(val);
				}

			private:
				template<class ...T>
				void init(std::tuple<T...>& types) {
					(std::get<T>(types)->init(), ...);
				}


			private:
				template<class ...T>
				using Tuple = std::tuple<std::shared_ptr<Type<T>>...>;
				Tuple<int, float, char> types;
			};
			Types types;

			// 番号の情報を持っているクラス
			class Content final
			{
			public:
				Content() = default;
				Content(int value) :value_(value) {}

			public:
				[[nodiscard]] int value() const noexcept { return value_; }

			private:
				int value_{};
			};
		}


		//---------------------------------------------------------------------------------
		template<typename Result, typename ...Arg>
		using Function = std::function<Result(Arg...)>;

		//---------------------------------------------------------------------------------
		bool Function1(int i) {
			return false;
		}
		//---------------------------------------------------------------------------------
		bool Function2(int i, int ii, std::string str) {
			return true;
		}

		//---------------------------------------------------------------------------------
		Function<bool, int>	Function3 = [](int i) {
			return true;
		};

		//---------------------------------------------------------------------------------
		Function<bool, int, std::string> Function4 = [](bool i, std::string str) {
			return true;
		};

		//---------------------------------------------------------------------------------
		core::util::CallBack<bool, int, std::string> Function5 = Function4;

		//---------------------------------------------------------------------------------
		core::util::CallBack<bool, int, std::string> FunctionCallback = [](int i, std::string str) {
			return true;
		};

		//---------------------------------------------------------------------------------
		std::unordered_map<std::string, core::util::CallBack<const char*>> FunctionMap;
#define INITIALIZE_SET(type)							\
				static const char* type##Name() {				\
					return #type;					            \
				}												\
																\
				type() {										\
					FunctionMap.emplace(#type, &type##Name);	\
				}


		//---------------------------------------------------------------------------------
		template <uint32_t hash>
		uint32_t getKeyHash() {
			return hash;
		}
#define KEY_HASH1(str) getKeyHash<core::util::stringToHash(str)>();

#define KEY_HASH2(str) []() {constexpr auto h = core::util::stringToHash(str); return h;}()


		//---------------------------------------------------------------------------------
		template<typename T>
		void checkFromArg(T x, std::enable_if_t<std::is_same<T, uint32_t>::value, std::nullptr_t> = nullptr) {
			TRACE(u8"ok");
		}
		void checkFromArg(...) {
			TRACE(u8"ng");
		}
		template<typename T, std::enable_if_t<std::is_same<T, uint32_t>::value, std::nullptr_t> = nullptr>
		void checkFromTempArg(T x) {
			TRACE(u8"ok");
		}
		void checkFromTempArg(...) {
			TRACE(u8"ng");
		}
		// スフィネで T 型が func を持っているかチェックする
		template<class T> struct has_method {
			template<typename _T>
			static auto check(...)->std::false_type;

			template<typename _T>
			static auto check(_T*) -> decltype(std::declval<_T>().func(), std::true_type());

			enum : bool { result = decltype(check<T>(nullptr))::value };
		};

		template<class T, class C>
		uint32_t findIndex(T&& t, const C& c) noexcept {
			if constexpr (std::is_trivial_v<T>) {
				if (auto find = std::find(c.begin(), c.end(), t); c.end() != find) {
					return static_cast<int>(std::distance(c.begin(), find));
				}
			}
			else {
				if (auto find = std::find_if(c.begin(), c.end(), [&](const auto& temp) { return temp.value() == t.value(); }); c.end() != find) {
					return static_cast<int>(std::distance(c.begin(), find));
				}
			}

			return -1;
		}


		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 */
		class Test
		{
		public:
			Test()
			{
				TRACE(u8"Test()");
			}

			Test(int i) {
				TRACE(u8"Test(int i)");
				m_value = i;
			}

			Test(const Test& r)
			{
				TRACE(u8"Test(const Test& r)");
			}

			Test(Test&& r) noexcept
			{
				TRACE(u8"Test(Test&& r)");
			}

			Test& operator=(const Test& r)
			{
				TRACE(u8"operator=(const Test& r)");
				return *this;
			}

			Test& operator=(Test&& r) noexcept
			{
				TRACE(u8"operator=(Test&& r)");
				return *this;
			}

			~Test()
			{
				TRACE(u8"  ~Test value [ %d ]", m_value);
			}

			void func() {

			}

		private:
			int m_value = 0;
		};


		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 */
		class TestTraceString
		{
		public:
			INITIALIZE_SET(TestTraceString)

		private:
			int m_value = 0;
		};


		//---------------------------------------------------------------------------------
		/**
		 * @brief	シーン初期化
		 * @return
		 */
		void SceneTest::Initialize() {
			//---------------------------------------------------------------------
			Function<bool, int>	func1;
			func1 = std::bind(&Function1, std::placeholders::_1);
			auto b1 = func1(1);

			Function<bool, int, int, std::string> func2;
			func2 = std::bind(&Function2, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
			auto b2 = func2(1, 2, "aaa");

			auto b3 = Function5(2, "bbb");

			//---------------------------------------------------------------------
			Test t1 = Test(1);
			Test t2 = Test(2);

			Test t3;
			t3 = std::move(t2);
			t3 = Test(3);

			TestTraceString t;
			for (const auto& f : FunctionMap)
			{
				TRACE(u8"%s", f.second());
			}

			test::types.func(1);
			test::types.func(1.3f);
			test::types.func('c');

			auto a = KEY_HASH1("aaaa");
			checkFromArg(a);
			checkFromArg("aaaa");
			checkFromTempArg(a);
			checkFromTempArg("aaaa");

			auto a2 = KEY_HASH2("bbb");

			auto b = has_method<Test>::result;
			if (b)
			{

			}


			std::vector<int> vector{ 1,2,3,4,5 };
			std::array<int, 5> arr{ 1,2,3,4,5 };
			std::vector<test::Content> container{ 1,2,3,4,5 };
			auto find1 = findIndex(3, vector);
			auto find2 = findIndex(2, arr);
			auto find3 = findIndex(test::Content(5), container);

			TRACE("%d, %d, %d", find1, find2, find3);

		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	シーン更新
		 * @return
		 */
		void SceneTest::Update() {

		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	シーン終了
		 * @return
		 */
		void SceneTest::Finalize() {

		}

	}
}
