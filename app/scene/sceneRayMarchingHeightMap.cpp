﻿#include "SceneRayMarchingHeightMap.h"

#include "../dx11/dx11.h"
#include "../buffer/buffer.h"

namespace app {

	namespace scene {

		//---------------------------------------------------------------------------------
		/**
		 * @brief	シーン初期化
		 * @return
		 */
		void SceneRayMarchingHeightMap::Initialize() {

			m_rayMarchingHeightMap.Initialize();
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	シーン更新
		 * @return
		 */
		void SceneRayMarchingHeightMap::Update() {

		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	シーン終了
		 * @return
		 */
		void SceneRayMarchingHeightMap::Finalize() {
		}
	}
}
