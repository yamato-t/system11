﻿#pragma once

#include "sceneBase.h"

#include "../material/textureMaterial.h"
#include "../2d/sprite.h"
#include "../3d/mesh.h"
#include "../3d/instance.h"
#include "../3d/plane.h"
#include "../post/mosaic.h"
#include "../post/snn.h"
#include "../post/blur.h"
#include "../post/nega.h"

namespace app {

	namespace scene{

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * シーン
		 */
		class SceneFilter : public SceneBase {
		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			SceneFilter() = default;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~SceneFilter() = default;

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	シーン初期化
			 * @return
			 */
			virtual void Initialize() override final;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	シーン更新
			 * @return
			 */
			virtual void Update() override final;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	シーン終了
			 * @return
			 */
			virtual void Finalize() override final;

		private:
			core::obj2d::Sprite		m_sprite;
			core::obj3d::Instance	m_inst;
			core::obj3d::Mesh		m_mesh;
			core::obj3d::Mesh		m_mesh2;
			core::filter::Mosaic	m_mosaic;
			core::filter::SNN		m_snn;
			core::filter::Blur		m_blur;
			core::filter::Nega		m_nega;

			core::material::TextureMaterial		m_material;
			core::obj3d::Plane					m_plane;

		};
	}
}
