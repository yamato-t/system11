﻿#pragma once

#include "sceneBase.h"

#include "../3d/instance.h"

namespace app {

	namespace scene{

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * シーン
		 */
		class SceneStandard : public SceneBase {
		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			SceneStandard() = default;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~SceneStandard() = default;

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	シーン初期化
			 * @return
			 */
			virtual void Initialize() override final;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	シーン更新
			 * @return
			 */
			virtual void Update() override final;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	シーン終了
			 * @return
			 */
			virtual void Finalize() override final;

		private:
//			core::obj3d::Instance	m_instance;
		};
	}
}
