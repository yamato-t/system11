﻿#include "sceneFilter.h"

#include "../dx11/dx11.h"
#include "../buffer/buffer.h"
#include "../screen/screen.h"

namespace app {

	namespace scene {

		//---------------------------------------------------------------------------------
		/**
		 * @brief	シーン初期化
		 * @return
		 */
		void SceneFilter::Initialize() {

			core::objScreen::Screen::Instance().GetVertexBuffer()->GetFormat();

			m_plane.Initialize();
			core::material::CreateTextureMaterialData data;
			data.texturePath = "resource/texture/photo.bmp";
			m_material.SetData(&data);
			m_material.SetShader("resource/hlsl/vertexTexture.hlsl", m_plane.GetVertexBuffer()->GetFormat());
			m_plane.SetMaterial(&m_material);

			//m_mosaic.Initialize();
			//m_snn.Initialize();
			m_blur.Initialize();
			//m_nega.Initialize();
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	シーン更新
		 * @return
		 */
		void SceneFilter::Update() {

		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	シーン終了
		 * @return
		 */
		void SceneFilter::Finalize() {
		}
	}
}
