﻿#pragma once

#include "sceneBase.h"

#include "../screen/rayMarchingHeightMap.h"

namespace app {

	namespace scene{

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * シーン
		 */
		class SceneRayMarchingHeightMap : public SceneBase {
		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			SceneRayMarchingHeightMap() = default;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~SceneRayMarchingHeightMap() = default;

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	シーン初期化
			 * @return
			 */
			virtual void Initialize() override final;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	シーン更新
			 * @return
			 */
			virtual void Update() override final;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	シーン終了
			 * @return
			 */
			virtual void Finalize() override final;

		private:
			core::objScreen::RayMarchingHeightMap		m_rayMarchingHeightMap;
		};
	}
}
