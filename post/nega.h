﻿#pragma once

#include "../obj/drawObj.h"
#include "proc.h"

#include "../dx11/dx11.h"
#include "../texture/texture.h"

namespace core {

	namespace filter {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * ネガポジ処理
		 */
		class Nega : public obj::Proc {

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			Nega() = default;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~Nega() = default;

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	初期化
			 * @return
			 */
			virtual void Initialize() override final;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ネガポジフィルタ描画
			 * @param	pContext	描画コンテキスト
			 * @return
			 */
			virtual void Draw(ID3D11DeviceContext* pContext) override final;

		private:
			dx11::ComPtr<ID3D11ComputeShader>	m_pCS;
			dx11::ComPtr<IUnorderedAccessView>	m_pUAV;
			core::texture::Texture				m_resource;

		};
	}
};
