﻿#pragma once

#include "../obj/drawObj.h"
#include "proc.h"

#include "../dx11/dx11.h"
#include "../texture/texture.h"

namespace core {

	namespace filter {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * SNN処理
		 */
		class SNN : public obj::Proc {

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			SNN() = default;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~SNN() = default;

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	初期化
			 * @return
			 */
			virtual void Initialize() override final;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	SNNフィルタ描画
			 * @param	pContext	描画コンテキスト
			 * @return
			 */
			virtual void Draw(ID3D11DeviceContext* pContext) override final;

		private:
			dx11::ComPtr<ID3D11ComputeShader>	m_pCS;
			dx11::ComPtr<IUnorderedAccessView>	m_pUAV;
			core::texture::Texture				m_resource;

		};
	}
};
