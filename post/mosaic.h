﻿#pragma once

#include "../obj/drawObj.h"
#include "proc.h"

#include "../dx11/dx11.h"
#include "../texture/texture.h"

namespace core {

	namespace filter {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * モザイクフィルタ処理
		 */
		class Mosaic : public obj::Proc {

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			Mosaic() = default;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~Mosaic() = default;

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	初期化
			 * @return
			 */
			virtual void Initialize() override final;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	モザイクフィルタ描画
			 * @param	pContext	描画コンテキスト
			 * @return
			 */
			virtual void Draw(ID3D11DeviceContext* pContext) override final;

		private:
			//dx11::ComPtr<IShaderResourceView>	m_pSRV;
			dx11::ComPtr<IUnorderedAccessView>	m_pUAV;
			dx11::ComPtr<ID3D11ComputeShader>	m_pCS;
			core::texture::Texture				m_resource;
		};
	}
};
