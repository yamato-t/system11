﻿#pragma once

#include "../obj/drawObj.h"
#include "proc.h"

#include "../dx11/dx11.h"
#include "../texture/renderTarget.h"

namespace core {

	namespace filter {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * ブラーフィルタ処理
		 */
		class Blur : public obj::Proc {
		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			Blur() = default;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~Blur() = default;

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	初期化
			 * @return
			 */
			virtual void Initialize() override final;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ブラーフィルタ描画
			 * @param	pContext	描画コンテキスト
			 * @return
			 */
			virtual void Draw(ID3D11DeviceContext* pContext) override final;

		private:
			texture::RenderTarget	m_target[4];		///< 縮小レンダーターゲット
			texture::RenderTarget	m_final;			///< 最終レンダーターゲット
		};
	}
};
