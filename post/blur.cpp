﻿#include "blur.h"
#include "../render/postProcRender.h"
#include "../render/obj3dRender.h"
#include "../win/window.h"
#include "../material/textureMaterial.h"

#include "../screen/screen.h"

namespace core {

	namespace filter {

		using namespace core::dx11;

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * コンスタントバッファ内容
		 */
		struct ConstantBufferFormat {
			vec4	param;	///< x,y:ブラーオフセット　z:基準ピクセルの重み
			vec4	weight;	///< 各サンプリングピクセルの重み
		};
		ConstantBufferFormat cBuffer;

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * ガウス計算
		 */
		float Gauss(vec2 pos, float rho) {
			return exp(-(pos.x * pos.x + pos.y * pos.y) / (2.0f * rho * rho));
		}
		
		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * コンスタントバッファ更新
		 */
		void UpdateCBuffer(ConstantBufferFormat& buffer, vec2 offset, float dispersion) {
			// オフセット設定
			buffer.param = vec4(offset.x, offset.y, 0, 0);

			// 重み
			buffer.param.z = Gauss(vec2(0, 0), dispersion);
			auto total = buffer.param.z;

			for (int i = 0; i < 4; ++i) {
				auto w = Gauss(offset * i, dispersion);
				buffer.weight[i] = w;
				total += (w * 2);
			}

			// 合計の重みが 1 になるように計算する
			buffer.param.z /= total;
			for (int i = 0; i < 4; ++i) {
				buffer.weight[i] /= total;
			}
		}


		//---------------------------------------------------------------------------------
		/**
		 * @brief	初期化
		 * @return
		 */
		void Blur::Initialize() {
			render::PostProcRender::Instance().Register(this);

			// 頂点バッファのフォーマットを定義
			uint32_t formats[] = {
				buffer::VertexBuffer::format::pos,
				buffer::VertexBuffer::format::uv,
			};

			// 頂点バッファの内容を作成
			struct IBuffer {
				vec3		pos;		// 位置
				vec2		uv;			// UV
			};
			IBuffer vertexBuffers[4];
			{
				vertexBuffers[0].pos = vec3(0.0f, 0.0f, 0);
				vertexBuffers[0].uv = vec2(0, 0);

				vertexBuffers[1].pos = vec3(0.0f, WINDOW_HEIGHT, 0);
				vertexBuffers[1].uv = vec2(0, 1);

				vertexBuffers[2].pos = vec3(WINDOW_WIDTH, 0.0f, 0);
				vertexBuffers[2].uv = vec2(1, 0);

				vertexBuffers[3].pos = vec3(WINDOW_WIDTH, WINDOW_HEIGHT, 0);
				vertexBuffers[3].uv = vec2(1, 1);
			}

			uint32_t formatCount = sizeof(formats) / sizeof(formats[0]);
			uint32_t vertexCount = sizeof(vertexBuffers) / sizeof(vertexBuffers[0]);
			m_vertexBuffer.Create(formatCount, formats, vertexCount, vertexBuffers);

			// インデックスバッファを作成
			m_pIndexBuffers = new buffer::IndexBuffer[1];
			uint16_t index[] = { 0, 1, 2, 3 };
			m_pIndexBuffers[0].Create(sizeof(uint16_t), vertexCount, index);

			// マテリアル作成
			m_materialNum = 2;
			m_pMaterials = new material::Material * [m_materialNum];
			m_pMaterials[0] = new material::TextureMaterial();
			m_pMaterials[1] = new material::TextureMaterial();
			// シェーダ設定
			m_pMaterials[0]->SetShader("resource/hlsl/blur.hlsl", m_vertexBuffer.GetFormat());
			m_pMaterials[1]->SetShader("resource/hlsl/blurCombine.hlsl", m_vertexBuffer.GetFormat());

			// レンダーターゲットの作成
			m_target[0].Create(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2, DXGI_FORMAT_R8G8B8A8_UNORM);
			// レンダーターゲットの作成
			m_target[1].Create(WINDOW_WIDTH / 4, WINDOW_HEIGHT / 4, DXGI_FORMAT_R8G8B8A8_UNORM);
			// レンダーターゲットの作成
			m_target[2].Create(WINDOW_WIDTH / 8, WINDOW_HEIGHT / 8, DXGI_FORMAT_R8G8B8A8_UNORM);
			// レンダーターゲットの作成
			m_target[3].Create(WINDOW_WIDTH / 16, WINDOW_HEIGHT / 16, DXGI_FORMAT_R8G8B8A8_UNORM);

			// レンダーターゲットの作成
			m_final.Create(WINDOW_WIDTH, WINDOW_HEIGHT, DXGI_FORMAT_R8G8B8A8_UNORM);

			// サンプラ(バイリニア)
			m_samplerState.Create(D3D11_TEXTURE_ADDRESS_CLAMP, D3D11_FILTER_MIN_MAG_LINEAR_MIP_POINT);

			// コンスタントバッファ作成
			m_constantBuffer.Create(sizeof(ConstantBufferFormat));

		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	ブラー処理
		 * @param	pContext	描画コンテキスト
		 * @return
		 */
		void Blur::Draw(ID3D11DeviceContext* pContext) {

			// 頂点バッファ設定
			auto vertex = &m_vertexBuffer;
			auto buffer = vertex->GetBuffer();
			UINT stride = vertex->Stride();
			UINT offset = vertex->Offset();
			pContext->IASetVertexBuffers(0, 1, &buffer, &stride, &offset);

			// プリミティブトポロジー設定
			pContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

			// インデックスバッファ設定
			auto index = GetIndexBuffer();
			auto indexBuffer = index->GetBuffer();
			pContext->IASetIndexBuffer(indexBuffer, index->GetDXGIFormat(), 0);

			auto colorBuffer = render::Obj3DRender::Instance().ColorTarget();

			// サンプラーステート設定
			ID3D11SamplerState* pSampler[1] = { m_samplerState.GetSamplerState() };
			pContext->PSSetSamplers(0, 1, pSampler);


			{
				// マテリアル設定
				m_pMaterials[0]->SetCommand(pContext);

				// シェーダリソース配列設定
				IShaderResourceView* pRes[][1] = {
					{ colorBuffer.GetTexture()->GetTextureView() },
					{ m_target[0].GetTexture()->GetTextureView() },
					{ m_target[1].GetTexture()->GetTextureView() },
					{ m_target[2].GetTexture()->GetTextureView() },
				};

				for (int i = 0; i < 4; ++i) {

					// レンダーターゲットの変更
					IRenderTargetView* targets[] = {
						m_target[i].GetRenderTargetView(),
					};
					pContext->OMSetRenderTargets(1, targets, nullptr);
					pContext->PSSetShaderResources(0, 1, pRes[i]);

					// ビューポート変更
					auto& state = render::Obj3DRender::Instance().GetRenderState();
					D3D11_TEXTURE2D_DESC textureDesc;
					m_target[i].GetTexture()->Get()->GetDesc(&textureDesc);
					state.SetViewport(pContext, vec2(0, 0), vec2(textureDesc.Width, textureDesc.Height));

					// コンスタントバッファ
					{
						auto c = (i % 2);
						const auto& offset = c == 1 ? vec2(2, 0) : vec2(0, 2);
						// 更新
						UpdateCBuffer(cBuffer, offset, 5);
						// Map/Unmap を利用している為、定数バッファが描画時に上書きされない
						// 本来は定数バッファを二つ用意するか、shaderを縦横で分けた方がいい
						D3D11_MAPPED_SUBRESOURCE	pData;
						ID3D11Buffer* pBuffer = m_constantBuffer.GetBuffer();
						if (SUCCEEDED(pContext->Map(pBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &pData))) {
							memcpy(pData.pData, (void*)&cBuffer, sizeof(ConstantBufferFormat));
							pContext->Unmap(pBuffer, 0);
						}
					}

					// ピクセルシェーダーにコンスタントバッファデータを渡す
					ID3D11Buffer* p[1] = { m_constantBuffer.GetBuffer() };
					pContext->PSSetConstantBuffers(0, 1, p);

					// インデックスバッファを利用した描画
					pContext->DrawIndexed(index->GetCount(), 0, 0);
				}
			}

			auto& state = render::Obj3DRender::Instance().GetRenderState();
			state.SetViewport(pContext, vec2(0, 0), vec2(WINDOW_WIDTH, WINDOW_HEIGHT));

			{
				// マテリアル設定
				m_pMaterials[1]->SetCommand(pContext);

				// レンダーターゲットの変更
				IRenderTargetView* targets[] = {
					m_final.GetRenderTargetView(),
				};
				pContext->OMSetRenderTargets(1, targets, nullptr);

				IShaderResourceView* pRes[] = {
					colorBuffer.GetTexture()->GetTextureView(),
					m_target[0].GetTexture()->GetTextureView(),
					m_target[1].GetTexture()->GetTextureView(),
					m_target[2].GetTexture()->GetTextureView(),
					m_target[3].GetTexture()->GetTextureView(),
				};
				pContext->PSSetShaderResources(0, 5, pRes);

				// インデックスバッファを利用した描画
				pContext->DrawIndexed(index->GetCount(), 0, 0);
			}

			// レンダーターゲットをバックバッファに戻す
			dx11::dx11Device::Instance().ResetRenderTarget(pContext);

			// 結果を表示
			vec4 rect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
			objScreen::Screen::Instance().DrawShaderResource(pContext, m_final.GetTexture()->GetTextureView(), &rect);
		}
	}
}
