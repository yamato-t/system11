#include "defuzzify.h"


// ---------------------------------------------------------------------
/*
*	非ファジー化の結果
*/
RESULT Defuzzify::Result(FuzzySet& outputFuzzySet) {
	RESULT res = NONE;

	auto wantHot		= outputFuzzySet[0].Value();
	auto wantCold		= outputFuzzySet[1].Value();
	auto best			= outputFuzzySet[2].Value();
	auto recommendHot	= outputFuzzySet[3].Value();
	auto recommendCold	= outputFuzzySet[4].Value();

	if(best < wantHot)
	{
		res = (recommendHot <= wantHot) ? TURN_UP : RECOMMEND_UP;
	} 
	else if(best < wantCold)
	{
		res = (recommendCold <= wantCold) ? TURN_DOWN : RECOMMEND_DOWN;
	}

	return res;
}