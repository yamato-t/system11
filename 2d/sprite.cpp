﻿#include "sprite.h"
#include "../render/obj2dRender.h"

namespace core {

	namespace obj2d {

		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		Sprite::Sprite() {
			// 描画登録
			obj::DrawObjContainer::Instance().Register(obj::DrawObjContainer::layer::ui, this);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		Sprite::~Sprite() {
			// 登録抹消
			//render::Obj2DRender::Instance().Erase(this);
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	スプライトの初期化
		 * @return
		 */
		void Sprite::Initialize() {

			// 頂点バッファのフォーマットを定義
			uint32_t formats[] = {
				buffer::VertexBuffer::format::pos,
				buffer::VertexBuffer::format::color,
			};

			// 頂点バッファの内容を作成
			struct IBuffer {
				vec3		pos;		// 位置
				vec4		color;		// カラー
			};

			// ピクセルに合わせた頂点設定なので、Yの上下が反転する事を考慮
			IBuffer vertexBuffers[4];
			{
				vertexBuffers[1].pos = vec3(0.0f, 0.0f, 0);
				vertexBuffers[1].color = vec4(1, 0, 0, 1);

				vertexBuffers[0].pos = vec3(0.0f, 128.0f, 0);
				vertexBuffers[0].color = vec4(0, 1, 0, 1);

				vertexBuffers[3].pos = vec3(128.0f, 0.0f, 0);
				vertexBuffers[3].color = vec4(0, 0, 1, 1);

				vertexBuffers[2].pos = vec3(128.0f, 128.0f, 0);
				vertexBuffers[2].color = vec4(0, 0, 0, 1);

			}
			uint32_t formatCount = sizeof(formats) / sizeof(formats[0]);
			uint32_t vertexCount = sizeof(vertexBuffers) / sizeof(vertexBuffers[0]);
			m_vertexBuffer.Create(formatCount, formats, vertexCount, vertexBuffers);

			// インデックスバッファを作成
			m_pIndexBuffers = new buffer::IndexBuffer[1];
			uint16_t index[] = { 0, 1, 2, 3 };
			m_pIndexBuffers[0].Create(sizeof(uint16_t), vertexCount, index);

			// マテリアル作成
			m_materialNum	= 1;
			m_pMaterials	= new material::Material*[m_materialNum];
			m_pMaterials[0]	= new material::Material();
			// シェーダ設定
			std::string shaderPath = "resource/hlsl/sprite.hlsl";
			m_pMaterials[0]->SetShader(shaderPath, m_vertexBuffer.GetFormat());

			m_prepared = true;
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	スプライトの描画
		 * @param	pContext	描画コンテキスト
		 * @param	pMaterial	外部指定のマテリアル
		 * @return
		 */
		void Sprite::Draw(ID3D11DeviceContext* pContext) {
			// 頂点バッファ設定
			auto vertex = GetVertexBuffer();
			auto buffer = vertex->GetBuffer();
			UINT stride = vertex->Stride();
			UINT offset = vertex->Offset();
			pContext->IASetVertexBuffers(0, 1, &buffer, &stride, &offset);

			// プリミティブトポロジー設定
			pContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

			// インデックスバッファ設定
			auto index			= GetIndexBuffer();
			auto indexBuffer	= index->GetBuffer();
			pContext->IASetIndexBuffer(indexBuffer, index->GetDXGIFormat(), 0);

			// マテリアル設定
			m_pMaterials[0]->SetCommand(pContext);

			// インデックスバッファを利用した描画
			pContext->DrawIndexed(index->GetCount(), 0, 0);
		}
	}
}
