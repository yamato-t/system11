
#include "common.hlsl"

// obj3dコンスタントバッファ スロット０
cbuffer primitive  : register(b0) {
	matrix	m_toWorld;			// ワールド空間への変換行列
	matrix	m_toObject;			// オブジェクト空間への変換行列
};


// テクスチャとサンプラ
Texture3D		m_texture	: register(t0);
SamplerState	m_sampler	: register(s0);

///////////////////////////////////////////////////////////////////////////////////////

// 頂点の入力レイアウト
struct VS_INPUT {
	float3 Pos		: IN_POSITION;
	float3 Uvw		: IN_UVW;
};


// 頂点の出力レイアウト
struct VS_OUTPUT {
	float4 Pos			: SV_POSITION;
	float3 Uvw			: TEXCOORD0;
	float4 WorldPos		: TEXCOORD1;
	float4 ScreenPos	: TEXCOORD2;
};

// 頂点シェーダー
VS_OUTPUT VS(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0;

	// 座標を一つ一つ変換するより、全行列を掛け合わせて一度計算する方が効率的
	matrix wv = mul(m_toWorld, camera.m_view);
	matrix wvp = mul(wv, camera.m_projection);
	output.Pos = mul(float4(input.Pos, 1), wvp);

	// スクリーン位置
	output.ScreenPos = output.Pos;

	// UV
	output.Uvw = input.Uvw;

	// ワールド座標（レイマーチ始点）
	output.WorldPos = mul(float4(input.Pos, 1), m_toWorld);

	return output;
}

///////////////////////////////////////////////////////////////////////////////////////


float3 Ray(float4 screenPos)
{
	float2 pos = screenPos.xy / screenPos.w;

	float3 cameraPos = camera.m_position.xyz;
	float3 cameraDir = normalize(camera.m_target.xyz - cameraPos);
	float3 cameraSide = normalize(cross(cameraDir, float3(0.0f, 1.0f, 0.0f)));
	float3 cameraUp = normalize(cross(cameraSide, cameraDir));

	return normalize(cameraSide * pos.x + cameraUp * -pos.y + cameraDir);
}

float3 ToLocal(float3 pos)
{
	return mul(float4(pos, 1), m_toObject).xyz;
}

bool IsInnerBox(float3 pos, float3 scale)
{
	//return
	//	abs(pos.x) < scale.x * 0.5 &&
	//	abs(pos.y) < scale.y * 0.5 &&
	//	abs(pos.z) < scale.z * 0.5;

	return all(max(scale * 0.5 - abs(pos), 0.0));
}

// ピクセルシェーダー
float4 PS(VS_OUTPUT input) : SV_TARGET
{
	float3 stride = 0.01f;
	float3 ray = Ray(input.ScreenPos) * stride;

	int    maxSteps = 100;
	float3 pos = input.WorldPos.xyz;

	float4 col = float4(0.0f, 0.0f, 0.0f, 0.0f);

	float boxSize = 2.0;
	for (int i = 0; i < maxSteps; i++)
	{
		float3 texCoord = ToLocal(pos);

		// 座標の正規化を行う
		texCoord = (texCoord + (boxSize * 0.5f)) * (boxSize * 0.25f);

		float4 c = m_texture.Sample(m_sampler, texCoord);
		if (0.1f < c.x)
		{
			col += c * 0.2f;
		}
		pos += ray;
		if (!IsInnerBox(ToLocal(pos), boxSize)) { break; }
	}

	return float4(col.x, col.x, col.x, col.x);
}


///////////////////////////////////////////////////////////////////////////////////////


// テクニック
technique10 SympleTec {
	pass P0 {
		SetVertexShader(CompileShader(vs_5_0, VS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_5_0, PS()));
	}
}