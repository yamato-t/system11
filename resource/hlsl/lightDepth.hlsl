#include "common.hlsl"

// obj3dコンスタントバッファ スロット０
cbuffer primitive  : register(b0) {
	matrix m_world;			// ワールド変換行列
};

///////////////////////////////////////////////////////////////////////////////////////

// 頂点の入力レイアウト
struct VS_INPUT {
	float3 Pos		: IN_POSITION;		// 座標
	float3 Normal	: IN_NORMAL;		// 法線
    float2 Uv		: IN_UV;			// UV
};

// 頂点の出力レイアウト
struct VS_OUTPUT {	
	float4 Pos			: SV_POSITION;
};

// 頂点シェーダー
VS_OUTPUT VS( VS_INPUT input )
{
	VS_OUTPUT output	= (VS_OUTPUT)0;

	// ライト深度
	matrix lightWv		= mul(m_world,					light.m_lightView);
	matrix lightWvp		= mul(lightWv,					light.m_lightOrtho);
	output.Pos			= mul(float4(input.Pos, 1),		lightWvp);

	return output;
}

///////////////////////////////////////////////////////////////////////////////////////

// 出力バッファ
struct PS_OUTPUT
{
    float4 Out0 : SV_Target0;	// MRT0への出力
};

// ピクセルシェーダー
PS_OUTPUT PS( VS_OUTPUT input ) : SV_TARGET
{
	PS_OUTPUT output;

	// 正射影なので深度は w で割る必要がない
	//output.Out0 = float4(input.Pos.z / input.Pos.w, 0.0f, 0.0f, 1.0f);
	output.Out0 = float4(input.Pos.z , 0.0f, 0.0f, 1.0f);

	return output;
}


///////////////////////////////////////////////////////////////////////////////////////


// テクニック
technique10 SympleTec {
	pass P0 {
		SetVertexShader(CompileShader(vs_4_0, VS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, PS()));
	}
}