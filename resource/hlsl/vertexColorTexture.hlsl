
#include "common.hlsl"

// obj3dコンスタントバッファ スロット０
cbuffer primitive  : register(b0) {
	matrix m_world;			// ワールド変換行列
};


// テクスチャとサンプラ
Texture2D		m_albedo	: register(t0);
SamplerState	m_sampler	: register(s0);

///////////////////////////////////////////////////////////////////////////////////////

// 頂点の入力レイアウト
struct VS_INPUT {
	float3 Pos		: IN_POSITION;
	float4 Color	: IN_COLOR;
	float2 Uv		: IN_UV;
};


// 頂点の出力レイアウト
struct VS_OUTPUT {
	float4 Pos		: SV_POSITION;
	float4 Color	: COLOR;
	float2 Uv		: TEXCOORD0;
};
 
// 頂点シェーダー
VS_OUTPUT VS( VS_INPUT input )
{
	VS_OUTPUT output	= (VS_OUTPUT)0;

	// 座標を一つ一つ変換するより、全行列を掛け合わせて一度計算する方が効率的
	matrix wv	= mul(m_world,				camera.m_view);
	matrix wvp	= mul(wv,					camera.m_projection);
	output.Pos	= mul(float4(input.Pos, 1), wvp);

	// カラー
	output.Color	= input.Color;

	// UV
	output.Uv		= input.Uv;


	return output;
}

///////////////////////////////////////////////////////////////////////////////////////


// ピクセルシェーダー
float4 PS( VS_OUTPUT input ) : SV_TARGET
{
	float4 albedo = m_albedo.Sample( m_sampler, input.Uv );
	return (albedo * input.Color);
}


///////////////////////////////////////////////////////////////////////////////////////


// テクニック
technique10 SympleTec {
	pass P0 {
		SetVertexShader(CompileShader(vs_4_0, VS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, PS()));
	}
}