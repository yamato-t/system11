
#include "common.hlsl"

// obj3dコンスタントバッファ スロット０
cbuffer primitive  : register(b0) {
	matrix	m_toWorld;			// ワールド空間への変換行列
	matrix	m_toObject;			// オブジェクト空間への変換行列
};


// テクスチャとサンプラ
Texture3D		m_texture	: register(t0);
SamplerState	m_sampler	: register(s0);

///////////////////////////////////////////////////////////////////////////////////////

// 頂点の入力レイアウト
struct VS_INPUT {
	float3 Pos		: IN_POSITION;
	float3 Uvw		: IN_UVW;
};


// 頂点の出力レイアウト
struct VS_OUTPUT {
	float4 Pos		: SV_POSITION;
	float3 Uvw		: TEXCOORD0;
};
 
// 頂点シェーダー
VS_OUTPUT VS( VS_INPUT input )
{
	VS_OUTPUT output	= (VS_OUTPUT)0;

	// 座標を一つ一つ変換するより、全行列を掛け合わせて一度計算する方が効率的
	matrix wv	= mul(m_toWorld,			camera.m_view);
	matrix wvp	= mul(wv,					camera.m_projection);
	output.Pos	= mul(float4(input.Pos, 1), wvp);

	// UV
	output.Uvw = input.Uvw;


	return output;
}

///////////////////////////////////////////////////////////////////////////////////////


// ピクセルシェーダー
float4 PS( VS_OUTPUT input ) : SV_TARGET
{
	float3 uvw = input.Uvw;
	float4 col = m_texture.Sample(m_sampler, uvw);
	return float4(col.r, col.r, col.r, 1.0f);
}


///////////////////////////////////////////////////////////////////////////////////////


// テクニック
technique10 SympleTec {
	pass P0 {
		SetVertexShader(CompileShader(vs_5_0, VS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_5_0, PS()));
	}
}