
#include "common.hlsl"

// コンスタントバッファ
cbuffer cb  : register(b0) {
	float4	param;		// x,y:ブラーオフセット　z:基準ピクセルの重み
	float4	weight;		// 各サンプリングピクセルの重み
};


// テクスチャとサンプラ
Texture2D		m_texture	: register(t0);
SamplerState	m_sampler	: register(s0);



///////////////////////////////////////////////////////////////////////////////////////

// 頂点の入力レイアウト
struct VS_INPUT {
	float3 Pos		: IN_POSITION;
	float2 Uv		: IN_UV;
};


// 頂点の出力レイアウト
struct VS_OUTPUT {
	float4 Pos		: SV_POSITION;
	float2 Uv		: TEXCOORD0;
};

// 頂点シェーダー
VS_OUTPUT VS(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0;

	output.Pos = mul(float4(input.Pos, 1), camera.m_projection);

	// UV
	output.Uv = input.Uv;

	return output;
}

///////////////////////////////////////////////////////////////////////////////////////


// ピクセルシェーダー
float4 PS(VS_OUTPUT input) : SV_TARGET
{
	float2 size;
	float  level;
	m_texture.GetDimensions(0, size.x, size.y, level);

	// 合成する
	float2 uv		= input.Uv + (float2(1.0f / size.x, 1.0f / size.y) * 0.5f);
	float2 uvOffset = param.xy / size;
	float4 color	= m_texture.Sample(m_sampler, uv) * param.z;
	color += (m_texture.Sample(m_sampler, uv + uvOffset)		  * weight.x);
	color += (m_texture.Sample(m_sampler, uv + (uvOffset * 2.0f)) * weight.y);
	color += (m_texture.Sample(m_sampler, uv + (uvOffset * 3.0f)) * weight.z);
	color += (m_texture.Sample(m_sampler, uv + (uvOffset * 4.0f)) * weight.w);
	color += (m_texture.Sample(m_sampler, uv - uvOffset)		  * weight.x);
	color += (m_texture.Sample(m_sampler, uv - (uvOffset * 2.0f)) * weight.y);
	color += (m_texture.Sample(m_sampler, uv - (uvOffset * 3.0f)) * weight.z);
	color += (m_texture.Sample(m_sampler, uv - (uvOffset * 4.0f)) * weight.w);

	return color;
}


///////////////////////////////////////////////////////////////////////////////////////


// テクニック
technique10 SympleTec {
	pass P0 {
		SetVertexShader(CompileShader(vs_5_0, VS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_5_0, PS()));
	}
}