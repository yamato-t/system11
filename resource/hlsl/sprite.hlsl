#include "common.hlsl"

///////////////////////////////////////////////////////////////////////////////////////

// 頂点の入力レイアウト
struct VS_INPUT {
	float3 Pos		: IN_POSITION;
	float4 Color	: IN_COLOR;
};


// 頂点の出力レイアウト
struct VS_OUTPUT {
	float4 Pos		: SV_POSITION;
	float4 Color	: COLOR;
};
 
// 頂点シェーダー
VS_OUTPUT VS( VS_INPUT input )
{
	VS_OUTPUT output	= (VS_OUTPUT)0;

	output.Pos	= mul(float4(input.Pos, 1), camera.m_projection);

	output.Color	= input.Color;

	return output;
}

///////////////////////////////////////////////////////////////////////////////////////


// ピクセルシェーダー
float4 PS( VS_OUTPUT input ) : SV_TARGET
{
	return input.Color;
}


///////////////////////////////////////////////////////////////////////////////////////


// テクニック
technique10 SympleTec {
	pass P0 {
		SetVertexShader(CompileShader(vs_4_0, VS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, PS()));
	}
}