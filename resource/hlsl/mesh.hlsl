#include "common.hlsl"

// obj3dコンスタントバッファ スロット０
cbuffer primitive  : register(b0) {
	matrix m_world;			// ワールド変換行列
};

// サンプラ
SamplerState	m_sampler	: register(s0);
// テクスチャ
Texture2D		m_albedo	: register(t0);
Texture2D		m_normalMap	: register(t1);

///////////////////////////////////////////////////////////////////////////////////////

// 頂点の入力レイアウト
struct VS_INPUT {
	float3 Pos		: IN_POSITION;		// 座標
	float3 Normal	: IN_NORMAL;		// 法線
    float2 Uv		: IN_UV;			// UV
};

// 頂点の出力レイアウト
struct VS_OUTPUT {	
	float4 Pos			: SV_POSITION;
	float3 Normal		: TEXCOORD0;
	float3 Tangent		: TEXCOORD1;
	float3 Binormal		: TEXCOORD2;
	float3 LightDir		: TEXCOORD3;
	float4 LightDepth	: TEXCOORD4;
	float2 Uv			: TEXCOORD5;
};

// 頂点シェーダー
VS_OUTPUT VS( VS_INPUT input )
{
	VS_OUTPUT output	= (VS_OUTPUT)0;

	matrix wv	= mul(m_world,				camera.m_view);
	matrix wvp	= mul(wv,					camera.m_projection);
	output.Pos	= mul(float4(input.Pos, 1), wvp);

	// ビュー空間における法線
	output.Normal	= normalize(mul(input.Normal, (float3x3)wv));
	// ビュー空間における接線（この計算では問題がある、正しく計算しなくてはならない）
	output.Tangent	= normalize(mul(cross(input.Normal, float3(0.0f, 1.0f, 0.000001f)), (float3x3)wv));
	// ビュー空間における従法線（DirectoX：左手座標系に合わせてTangentを反転）
	output.Binormal	= normalize(cross(output.Normal, (output.Tangent * -1)));

	// ビュー空間におけるライト向き
	output.LightDir	= normalize(mul(light.m_dir.xyz, (float3x3)camera.m_view));

	// ライト深度
	matrix lightWv		= mul(m_world,					light.m_lightView);
	matrix lightWvp		= mul(lightWv,					light.m_lightOrtho);
	output.LightDepth	= mul(float4(input.Pos, 1),		lightWvp);

	// UV
	output.Uv		= input.Uv;


	return output;
}

///////////////////////////////////////////////////////////////////////////////////////

// 出力バッファ
struct PS_OUTPUT
{
    float4 Out0 : SV_Target0;	// MRT0への出力
    float4 Out1 : SV_Target1;	// MRT1への出力
};

// ピクセルシェーダー
PS_OUTPUT PS( VS_OUTPUT input ) : SV_TARGET
{
	PS_OUTPUT output;

	// 法線マップの反映
	float3 normal = CalcNormal(input.Tangent, input.Binormal, input.Normal, m_normalMap.Sample( m_sampler, input.Uv).xyz);

	// カラー
	float lightPow	= pow(CalcHalfLambertDiffusePower(normal, -input.LightDir), 3.0f);
	output.Out0		= m_albedo.Sample( m_sampler, input.Uv ) * lightPow;

	// シャドウマップ反映
	// 頂点計算の結果から、このピクセルにおけるライト深度を再計算
	float	bias			= 0.0005f;
	// 正射影なので深度は w で割る必要がない
	float	lightDepth		= (input.LightDepth.z/* / input.LightDepth.w*/);
	float2	shadowMapCoord	= CalcTexCoord(input.LightDepth);
	float	shadowMapDepth	= m_shadowMap.Sample(m_shadowMapSampler, shadowMapCoord).r + bias;

	// 二つのライト情報を見比べて影か否か判断する
	if(shadowMapDepth <= lightDepth) { output.Out0 *= 0.1f; }
	output.Out0.a	= 1.0f;


	output.Out1 = float4((normal + 1) * 0.5f, 1.0f);

	return output;
}


///////////////////////////////////////////////////////////////////////////////////////


// テクニック
technique10 SympleTec {
	pass P0 {
		SetVertexShader(CompileShader(vs_4_0, VS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, PS()));
	}
}