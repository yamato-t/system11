
#include "common.hlsl"


// テクスチャとサンプラ
Texture2D		m_texture0	: register(t0);
Texture2D		m_texture1	: register(t1);
Texture2D		m_texture2	: register(t2);
Texture2D		m_texture3	: register(t3);
Texture2D		m_texture4	: register(t4);
SamplerState	m_sampler	: register(s0);




///////////////////////////////////////////////////////////////////////////////////////

// 頂点の入力レイアウト
struct VS_INPUT {
	float3 Pos		: IN_POSITION;
	float2 Uv		: IN_UV;
};


// 頂点の出力レイアウト
struct VS_OUTPUT {
	float4 Pos		: SV_POSITION;
	float2 Uv		: TEXCOORD0;
};

// 頂点シェーダー
VS_OUTPUT VS(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0;

	output.Pos = mul(float4(input.Pos, 1), camera.m_projection);

	// UV
	output.Uv = input.Uv;


	return output;
}

///////////////////////////////////////////////////////////////////////////////////////


// ピクセルシェーダー
float4 PS(VS_OUTPUT input) : SV_TARGET
{
	float4 sub	= float4(0.6f, 0.6f, 0.6f, 0);
	float4 sub2 = float4(0.02f, 0.02f, 0.02f, 0);
	float4 color = m_texture0.Sample(m_sampler, input.Uv)
	+ ((m_texture1.Sample(m_sampler, input.Uv) - sub) * 0.01f)
	+ ((m_texture2.Sample(m_sampler, input.Uv) - sub) * 0.05f)
	+ ((m_texture3.Sample(m_sampler, input.Uv) - sub) * 0.2f )
	+ ((m_texture4.Sample(m_sampler, input.Uv) - sub2));

	return color;
}


///////////////////////////////////////////////////////////////////////////////////////


// テクニック
technique10 SympleTec {
	pass P0 {
		SetVertexShader(CompileShader(vs_4_0, VS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, PS()));
	}
}