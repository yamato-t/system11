#pragma once

#include "dx11def.h"
#include "singleton.h"

namespace core 
{
	namespace dx11 
	{
		//---------------------------------------------------------------------------------
		/**
		 * @brief	
		 * DirectX11デバイスの制御
		 */
		class dx11Device : public util::Singleton<dx11Device>
		{
		private:
			friend class util::Singleton<dx11Device>;

		private:
			class cImpl;
			std::shared_ptr<cImpl> m_pImpl;		///< インプリメントクラスポインタ

		private:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			dx11Device();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~dx11Device();

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	デバイスの初期化
			 * @return
			 */
			void dx11Device::Initialize();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	レンダーターゲットにバックバッファを設定
			 * @param	pContext	描画コンテキスト
			 * @return
			 */
			void ResetRenderTarget(ID3D11DeviceContext* pContext);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	バックバッファのクリア
			 * @param	pContext	描画コンテキスト
			 * @return
			 */
			void Clear(ID3D11DeviceContext* pContext);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	フロントバッファへ出力
			 * @return
			 */
			void dx11Device::Present();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デバイス取得
			 * @return	デバイスのポインタ
			 */
			ID3D11Device* Device();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デバイスコンテキストの取得
			 * @return	デバイスコンテキストのポインタ
			 */
			ID3D11DeviceContext* dx11Device::DeviceContext();


			//---------------------------------------------------------------------------------
			/**
			 * @brief	GPU処理が終了するまで待機
			 * @return
			 */
			void WaitGPU();
		};
	}
};