﻿#pragma once

#include <d3d11.h>
#include <DXGI.h>
#include <DirectXMath.h>
#include <d3dcompiler.h>

#pragma comment(lib,"d3d11.lib")
#pragma comment(lib,"d3dcompiler.lib")
#pragma comment(lib,"DXGI.lib")

namespace core
{
	namespace dx11
	{
#define SAFE_COM_RELEASE(x) if(x) { x->Release(); x = nullptr; }

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * COMオブジェクト（ID3D関連）スマートポインタ
		 */
		template <class T>
		class ComPtr
		{
		private:
			T* p;		///< 指定（ID3D関連）型のポインタ

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			ComPtr() { p = nullptr; }

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~ComPtr() { Reset(); }

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ポインタの設定
			 * @param	pp		指定型のポインタ
			 * @return
			 */
			void Reset(T* pp = nullptr)
			{
				SAFE_COM_RELEASE(p);

				p = pp;
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ポインタ取得のオペレータ
			 * @return	指定型のポインタ
			 */
			T* operator->() { return p; }

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ポインタ直接取得
			 * @return	指定型のポインタ
			 */
			T* GetPointer() { return p; }

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ポインタのポインタ取得
			 * @return	指定型のポインタのポインタ
			 */
			T** GetPointerPointer() { return &p; }

		private:
			// その他コンストラクタを許可しない
			ComPtr(const ComPtr& r) = delete;
			ComPtr& operator=(const ComPtr& r) = delete;
			ComPtr(ComPtr&& r) = delete;
			ComPtr& operator=(ComPtr&& r) = delete;
		};
	}
}

using ITexture2D = ID3D11Texture2D;
using ITexture3D = ID3D11Texture3D;
using IShaderResourceView = ID3D11ShaderResourceView;
using IUnorderedAccessView = ID3D11UnorderedAccessView;
using IRenderTargetView = ID3D11RenderTargetView;
using IDepthStencilView = ID3D11DepthStencilView;
using IBuffer = ID3D11Buffer;
