﻿#pragma once

#include "../dx11/dx11.h"
#include "../math/math.h"
#include "../buffer/vertexBuffer.h"

namespace core
{
	namespace shader
	{
		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * シェーダ制御クラス
		 */
		class Shader {
		private:
			class cImpl;
			std::shared_ptr<cImpl> m_pImpl;	///< インプリメントクラスポインタ

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			Shader();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			~Shader();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	シェーダの生成
			 * @param	path			シェーダファイルのパス
			 * @param	formats	頂点バッファフォーマット
			 * @return
			 */
			void Create(const std::string& path, const std::vector<buffer::VertexBuffer::format>& formats);


			//---------------------------------------------------------------------------------
			/**
			 * @brief	シェーダの生成
			 * @param	path			シェーダファイルのパス
			 * @param	formats			頂点バッファフォーマット
			 * @param	instanceFormats	インスタンシング用追加頂点バッファのフォーマット
			 * @return
			 */
			void Create(const std::string& path, const std::vector<buffer::VertexBuffer::format>& formats, const std::vector<buffer::VertexBuffer::format>& instanceFormats);

			//---------------------------------------------------------------------------------
			/**
			 * @brief	頂点シェーダ取得
			 * @return	頂点シェーダ制御のポインタ
			 */
			ID3D11VertexShader*	VertexShader();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ピクセルシェーダ取得
			 * @return	ピクセルシェーダ制御のポインタ
			 */
			ID3D11PixelShader*	PixelShader();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	頂点レイアウト取得
			 * @return	設定された頂点バッファを元に作成した頂点レイアウト
			 */
			ID3D11InputLayout*	VertexLayout();
		};
	}

};
