﻿#pragma once

namespace core {

	namespace util {

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * コールバック
		 */
		template<typename Result, typename ...Arg>
		class CallBack {
		public:
			using Function = std::function<Result(Arg...)>;

			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			CallBack()
			{
				m_function = nullptr;
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			CallBack(const Function& f)
			{
				m_function = f;
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			CallBack(Function&& f)
			{
				m_function = nullptr;
				m_function = std::move(f);
				f = nullptr;
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~CallBack() {}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	代入
			 */
			Function& operator=(const Function& f)
			{
				m_function = f;
				return m_function;
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	代入
			 */
			Function& operator=(Function&& f)
			{
				m_function = nullptr;
				m_function = std::move(f);
				f = nullptr;
				return m_function;
			}


			//---------------------------------------------------------------------------------
			/**
			 * @brief	実行
			 */
			Result operator()(Arg ...arg) const
			{
				ASSERT(m_function, "不正な関数です");
				return m_function(arg ...);
			}

		private:
			Function m_function;
		};
	}
}
