﻿#include "framerate.h"

namespace core {
	namespace util {
		using namespace std;

		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * フレームレート制御のインプリメントクラス
		 */
		class FrameRate::cImpl {
		public:

			cImpl()	{
				SetFPS(30);
			}
			~cImpl() {}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	FPSを設定する
			 * @param	1秒当たりのフレーム数
			 * @return
			 */
			void SetFPS(float framePerSecond) {
				m_framerate = framePerSecond;
				m_wait		= long long((1.0f / m_framerate) * 1000.0f * 1000.0f);

				auto now		= chrono::system_clock::now().time_since_epoch();
				m_prevMicroSec	= chrono::duration_cast<chrono::microseconds>(now);
			}

			//---------------------------------------------------------------------------------
			/**
			 * @brief	フレームを待つ
			 * @return
			 */
			void WaitFrame() {

				{
					auto now		= chrono::system_clock::now().time_since_epoch();
					auto current	= chrono::duration_cast<chrono::microseconds>(now);
					auto elapsed	= (current - m_prevMicroSec).count();

					if (elapsed < m_wait) {
						this_thread::sleep_for(chrono::microseconds(m_wait - elapsed));
					}
				}

				{
					auto now		= chrono::system_clock::now().time_since_epoch();
					m_prevMicroSec	= chrono::duration_cast<chrono::microseconds>(now);
				}
			}

		private:
			float					m_framerate		= 60.0f;	///!< 一秒間のフレーム数
			long long 				m_wait			= 0;		///!< 必要な待機時間
			chrono::microseconds	m_prevMicroSec;				///!< 保存されたマイクロ秒
		};


		//---------------------------------------------------------------------------------
		/**
		 * @brief	コンストラクタ
		 */
		FrameRate::FrameRate() {
			m_pImpl.reset(new FrameRate::cImpl());
		}


		//---------------------------------------------------------------------------------
		/**
		 * @brief	デストラクタ
		 */
		FrameRate::~FrameRate() {
		}

		//---------------------------------------------------------------------------------
		/**
		 * @brief	FPSを設定する
		 * @param	1秒当たりのフレーム数
		 * @return
		 */
		void FrameRate::SetFPS(float framePerSecond) {
			m_pImpl->SetFPS(framePerSecond);
		}


		//---------------------------------------------------------------------------------
		/**
		 * @brief	フレームを待つ
		 * @return
		 */
		void FrameRate::WaitFrame() {
			m_pImpl->WaitFrame();
		}

	}
}
