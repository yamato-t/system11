#pragma once

/** @def
 * アラインメントサイズ
 */
#define ALIGN_SIZE	(4)

/** @def
 * アラインメント
 */
#define ALIGN(size)	((size + ALIGN_SIZE - 1) & ~(ALIGN_SIZE - 1))
