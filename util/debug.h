﻿#pragma once

namespace core {

	namespace util {

		//---------------------------------------------------------------------------------
		/**
		 * @brief デバッグトレース表示
		 * @param	str	表示文字列
		 * @return	
		 */
		void DebugString(const char * str, ...);


		//---------------------------------------------------------------------------------
		/**
		 * @brief アサート
		 * @param	condition	アサート条件
		 * @param	str			表示文字列
		 * @return	
		 */
		void Assert(bool condition, const char * str, ...);
	}
}


#if DEBUG
#define TRACE(str, ...)				core::util::DebugString(str, __VA_ARGS__)
#define ASSERT(condition, str, ...)	core::util::Assert(!!condition, str, __VA_ARGS__)
#else
#define TRACE(str, ...)
#define ASSERT(condition, str, ...)
#endif