﻿#pragma once

#include "singleton.h"

namespace core {

	namespace util {


		//---------------------------------------------------------------------------------
		/**
		 * @brief
		 * フレームレート制御
		 *
		 * シングルトンによる制御
		 */
		class FrameRate : public Singleton<FrameRate> {
		private:
			friend class Singleton<FrameRate>;

		private:
			class cImpl;
			std::shared_ptr<cImpl> m_pImpl;		///< インプリメントクラスポインタ


		private:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コンストラクタ
			 */
			FrameRate();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			~FrameRate();

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	FPSを設定する
			 * @param	1秒当たりのフレーム数
			 * @return
			 */
			void SetFPS(float framePerSecond);


			//---------------------------------------------------------------------------------
			/**
			 * @brief	フレームを待つ
			 * @return
			 */
			void WaitFrame();
		};
	}
}

#define FRAME_RATE_VALID()		(core::util::FrameRate::IsValid())
#define FRAME_RATE()			(core::util::FrameRate::Instance())
