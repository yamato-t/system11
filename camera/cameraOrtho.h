#pragma once

#include "camera.h"

namespace core {

	namespace camera {

		//---------------------------------------------------------------------------------
		/**
		 * @brief	
		 * 正射影カメラの制御
		 */
		class CameraOrtho : public Camera {

		public:
			//---------------------------------------------------------------------------------
			/**
			 * @brief	コントラクタ
			 */
			CameraOrtho();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	デストラクタ
			 */
			virtual ~CameraOrtho();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	ビュー行列とプロジェクション行列の更新
			 * @return
			 */
			virtual void UpdateViewProjection();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	スクリーン投影のビューとプロジェクション行列の更新
			 * @return
			 */
			void UpdateScreenViewProjection();

			//---------------------------------------------------------------------------------
			/**
			 * @brief	正射影カメラのコンスタントバッファを更新
			 * @param	pContext	描画コンテキスト
			 * @return
			 */
			virtual void UpdateConstantBuffer(ID3D11DeviceContext* pContext);

		};
	}
};